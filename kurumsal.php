<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $lang['corporate']; ?> - <?php echo $lang['ihracatin']; ?></title>
    <?php include 'includes/styles.php'; ?>
</head>
<body>
<?php include 'includes/header.php'; ?>
<section class="pageheader">
    <div class="page-header-content">
        <div class="page-header-content-box">
            <h3>HAKKIMIZDA</h3>
            <p>
            Accumulate, hedef ve beklentilerinizi en doğru şekilde belirledikten sonra sizin ihtiyacınız olan özgün stratejiyi oluşturur. 
            </p>
        </div>
        <div class="bottom"></div>
    </div>
    <img src="assets/images/pageheaders/aboutus-pageheader.jpg" alt="" />
    <div class="page-header-bottom">
        <div class="page-header-bottom-inside">
            <div class="container">
                <div class="page-header-menu">
                    <ul>
                        <li><a href="index.php">Ana Sayfa &nbsp; ></a></li>
                        <li><a >Hakkımızda</a></li>
                    </ul>
                </div>
            </div> 
        </div>
    </div>
</section>
<section class="main-content">
    <div class="container">
       <div class="page-content text-center">
           <div class="row">
               <div class="col-md-2"></div>
               <div class="col-md-8">
               <p>Accumulate ekibi olarak dijital dünyadaki deneyimlerimizi kullanarak ve sürekli gelişen teknolojiye ayak uydurarak sizlere yardımcı oluyoruz. Markaların kendine özgü dünyalarını dikkate alıp, hedef ve beklentilerini doğru belirledikten sonra stratejiyi birlikte oluşturuyor, her geçen gün artan rekabet koşullarında hedeflerinize yönelik işlerle kısa sürede birlikte büyüyoruz.</p>
        <p>Markalar için ürettiğimiz yaratıcı stratejiler doğrultusunda hedef kitleye uygun, dikkat çekici reklamlar için en doğru modelleri kullanıyoruz. Detaylara önem veren, proaktif yapımızla ihtiyaçlarınızı tam isabetle tespit ediyor, çalışmalarımızla markanızı geliştirmeye olanak sağlıyoruz. Tüm bu süreçlerde verimliliği ve bütçe değerlendirmesini de çok önemsiyor, anlık raporlamalarla tüm kampanyaları birlikte yönetiyoruz.</p>
        <p>Dijital dünyada mecralar çoğalırken görünürlük azalıyor; marka mesajları yığının içinde kaybolma tehlikesi yaşıyor. Uygun hedeflemeyle birlikte doğru ve verimli reklam modelleri bu dünyada var olmanın ilk koşulu haline geliyor. Siz de bu dijital dünyada başarılarla dolu bir yolculuğa başlamak isterseniz, sizi bekliyoruz. 
</p>
               </div>
               <div class="col-md-2"></div>
           </div>

       </div>
    </div>
</section>

<?php include 'includes/footer.php'; ?>

<?php include 'includes/scripts.php'; ?>
</body>
</html>
