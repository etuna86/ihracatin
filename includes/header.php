<header>
    <div class="main-header">
        <div class="row">
            <div class="col-md-1">
                <div class="logo-section">
                    <a href="index.php">
                        <img class="logo-white" src="assets/images/logo-white.svg" alt="" />
                        <img class="logo-black"  src="assets/images/logo.svg" alt="" />

                    </a>
                </div>
            </div>
            <div class="col-md-11">
               <nav class="main-menu">
                    <ul>
                        <li class="sub-menu-title"><a href="#"><?php echo $lang['exporter_solution']; ?></a>
                            <ul>
                                <li><a class="list" href="index.php#exporters"><?php echo $lang['exporters']; ?></a></li>
                                <li><a class="list" href="index.php#exporters2"><?php echo $lang['thinking_export']; ?></a></li>
                                <li><a class="list" href="index.php#exporters3"><?php echo $lang['e_export']; ?></a></li>
                            </ul>
                        </li>
                        <li class="sub-menu-title" ><a href="#"><?php echo $lang['products']; ?></a>
                            <ul>
                                <li><a href="google-marketfinder.php"><?php echo $lang['google_marketfinder']; ?></a></li>
                                <li><a href="theadx_export.php"><?php echo $lang['theadx_export']; ?></a></li>
                                <li><a href="together-social.php"><?php echo $lang['together_social']; ?></a></li>
                                <li><a href="media-planning.php"><?php echo $lang['media_planning']; ?></a></li>
                                <li><a href="online-sales-site.php"><?php echo $lang['online_sale_site']; ?></a></li>
                                <li><a href="data-management.php"><?php echo $lang['data_management']; ?></a></li>
                                <li><a href="web-mobil-development.php"><?php echo $lang['web_mobil_application_develop']; ?></a></li>
                                <li><a href="e-marketing-management.php"><?php echo $lang['emarketing_management']; ?></a></li>
                                <li><a href="social-media-export.php"><?php echo $lang['facebook_export']; ?></a></li>
                            </ul>
                        </li>
                        <li class="sub-menu-title" ><a href="#"><?php echo $lang['we_doing']; ?></a>
                            <ul>

                                <li><a class="list"  href="index.php#marketresearch"><?php echo $lang['market_research']; ?></a></li>
                                <li><a class="list" href="index.php#marketresearch"><?php echo $lang['planning_with_ai']; ?></a></li>
                                <li><a class="list" href="index.php#customerdata"><?php echo $lang['customer_data']; ?></a></li>
                                <li><a class="list" href="index.php#datamining"><?php echo $lang['data_mining']; ?></a></li>
                                <li><a class="list" href="index.php#createbrand"><?php echo $lang['create_brand']; ?></a></li>
                            </ul>
                        </li>
                        <li><a href="export_consultants.php"><?php echo $lang['export_consultants']; ?></a></li>
                        <li><a href="referances.php"><?php echo $lang['collaborators']; ?></a></li>
                        <li><a href="partners.php"><?php echo $lang['partners']; ?></a></li>
                        <li><a href="https://kolaydestek.gov.tr/destekler/2/24" target="_blank"><?php echo $lang['government_supports']; ?></a></li>
                        <li><a href="contact.php"><?php echo $lang['contact']; ?></a></li>
                        <li class="lang-section">
                            <div class="dropdown language">
                                <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php 	if (!isset($_SESSION["lang"])){
                                        echo "tr";
                                    }else {
                                        echo $_SESSION["lang"];
                                    } ?>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="lang/index.php?lang=tr">tr</a>
                                    <a class="dropdown-item" href="lang/index.php?lang=en">en</a>
                                </div>
                            </div>
                        </li>
                        <li class="application-btn"  data-toggle="modal" data-target="#exampleModal" ><a href="#" class="quickapplyone" > <?php echo $lang['quick_application']; ?></a></li>
                    </ul>

                </nav>
                <div class="mobile-lang">
                    <div class="dropdown language">
                        <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php 	if (!isset($_SESSION["lang"])){
                                echo "tr";
                            }else {
                                echo $_SESSION["lang"];
                            } ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="lang/index.php?lang=tr">tr</a>
                            <a class="dropdown-item" href="lang/index.php?lang=en">en</a>
                        </div>
                    </div>
                </div>
                <div class="mobil-menu-btn toggleBodyNoScroll">
                    <div class="top-line"></div>
                    <div class="middle-line"></div>
                    <div class="bottom-line"></div>
                </div>
               
                <nav class="mobile-menu">
                    <ul>
                        <li class="sub-menu-title mobile-submenu-btn">
                        <span class="triangle-block"><i class="fa fa-angle-right"></i></span>     
                        <a href="#"><?php echo $lang['exporter_solution']; ?></a>
                            <ul class="mobile-submenu">
                            <li><a class="list" href="index.php#exporters"><?php echo $lang['exporters']; ?></a></li>
                                <li><a class="list" href="index.php#exporters2"><?php echo $lang['thinking_export']; ?></a></li>
                                <li><a class="list" href="index.php#exporters3"><?php echo $lang['e_export']; ?></a></li>
                            </ul>
                        </li>
                        <li class="sub-menu-title mobile-submenu-btn " >
                        <span class="triangle-block "><i class="fa fa-angle-right"></i></span>     
                        <a href="#"><?php echo $lang['products']; ?></a>
                            <ul class="mobile-submenu">
                            <li><a href="google-marketfinder.php"><?php echo $lang['google_marketfinder']; ?></a></li>
                                <li><a href="theadx_export.php"><?php echo $lang['theadx_export']; ?></a></li>
                                <li><a href="together-social.php"><?php echo $lang['together_social']; ?></a></li>
                                <li><a href="media-planning.php"><?php echo $lang['media_planning']; ?></a></li>
                                <li><a href="online-sales-site.php"><?php echo $lang['online_sale_site']; ?></a></li>
                                <li><a href="data-management.php"><?php echo $lang['data_management']; ?></a></li>
                                <li><a href="web-mobil-development.php"><?php echo $lang['web_mobil_application_develop']; ?></a></li>
                                <li><a href="e-marketing-management.php"><?php echo $lang['emarketing_management']; ?></a></li>
                                <li><a href="social-media-export.php"><?php echo $lang['facebook_export']; ?></a></li>
                            </ul>
                        </li>
                        <li class="sub-menu-title mobile-submenu-btn" >
                        <span class="triangle-block mobile-submenu-btn"><i class="fa fa-angle-right"></i></span>     
                        <a href="#">Ne Yapıyoruz ?</a>
                            <ul class="mobile-submenu">
                            <li><a class="list"  href="index.php#marketresearch"><?php echo $lang['market_research']; ?></a></li>
                                <li><a class="list" href="index.php#marketresearch"><?php echo $lang['planning_with_ai']; ?></a></li>
                                <li><a class="list" href="index.php#customerdata"><?php echo $lang['customer_data']; ?></a></li>
                                <li><a class="list" href="index.php#datamining"><?php echo $lang['data_mining']; ?></a></li>
                                <li><a class="list" href="index.php#createbrand"><?php echo $lang['create_brand']; ?></a></li>
                         
                            </ul>
                        </li>
                        <li><a href="export_consultants.php"><?php echo $lang['export_consultants']; ?></a></li>
                        <li><a href="referances.php"><?php echo $lang['collaborators']; ?></a></li>
                        <li><a href="partners.php"><?php echo $lang['partners']; ?></a></li>
                        <li><a href="https://kolaydestek.gov.tr/destekler/2/24" target="_blank"><?php echo $lang['government_supports']; ?></a></li>
                        <li><a href="contact.php"><?php echo $lang['contact']; ?></a></li>
                        <li class="application-btn"  ><a href="#" data-toggle="modal" data-target="#exampleModal"  class="quickapplyone" ><?php echo $lang['quick_application']; ?></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>