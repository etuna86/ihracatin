<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-logo">
                    <h3><?php echo $lang['ihracatin']; ?></h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="footer-menu">
                    <h3><?php echo $lang['exporter_solution']; ?></h3>
                    <ul>
                        <li><a class="list" href="index.php#exporters"><?php echo $lang['exporters']; ?></a></li>
                        <li><a class="list" href="index.php#exporters2"><?php echo $lang['thinking_export']; ?></a></li>
                        <li><a class="list" href="index.php#exporters3"><?php echo $lang['e_export']; ?></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="footer-menu">
                    <h3><?php echo $lang['products']; ?></h3>
                    <ul>
                    <li><a href="google-marketfinder.php"><?php echo $lang['google_marketfinder']; ?></a></li>
                                <li><a href="theadx_export.php"><?php echo $lang['theadx_export']; ?></a></li>
                                <li><a href="together-social.php"><?php echo $lang['together_social']; ?></a></li>
                                <li><a href="media-planning.php"><?php echo $lang['media_planning']; ?></a></li>
                                <li><a href="online-sales-site.php"><?php echo $lang['online_sale_site']; ?></a></li>
                                <li><a href="data-management.php"><?php echo $lang['data_management']; ?></a></li>
                                <li><a href="web-mobil-development.php"><?php echo $lang['web_mobil_application_develop']; ?></a></li>
                                <li><a href="e-marketing-management.php"><?php echo $lang['emarketing_management']; ?></a></li>
                                <li><a href="social-media-export.php"><?php echo $lang['facebook_export']; ?></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="footer-menu">
                    <h3><?php echo $lang['we_doing']; ?></h3>
                    <ul>
                        <li><a class="list"  href="index.php#marketresearch"><?php echo $lang['market_research']; ?></a></li>
                        <li><a class="list" href="index.php#marketresearch"><?php echo $lang['planning_with_ai']; ?></a></li>
                        <li><a class="list" href="index.php#customerdata"><?php echo $lang['customer_data']; ?></a></li>
                        <li><a class="list" href="index.php#datamining"><?php echo $lang['data_mining']; ?></a></li>
                        <li><a class="list" href="index.php#createbrand"><?php echo $lang['create_brand']; ?></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="footer-menu last-menu">
                    <ul>
                        <li><a href="export_consultants.php"><?php echo $lang['export_consultants']; ?></a></li>
                        <li><a href="referances.php"><?php echo $lang['collaborators']; ?></a></li>
                        <li><a href="partners.php"><?php echo $lang['partners']; ?></a></li>
                        <li><a href="https://kolaydestek.gov.tr/destekler/2/24" target="_blank"><?php echo $lang['government_supports']; ?></a></li>
                        <li><a href="contact.php"><?php echo $lang['contact']; ?></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="footer-social">
                    <h3><?php echo $lang['socialmedia_accounts']; ?> </h3>
                    <ul>
                        <li><a href="https://www.linkedin.com/company/i%CC%87hracat-in/?viewAsMember=true" target="_blank" ><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>
                    <div class="follow-us"><?php echo $lang['youcan_followus']; ?></div>
                </div> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="footer-bottom">
                    <div class="row">
                        <div class="col-md-3">

                        </div>
                        <div class="col-md-5">
                            <div class="footer-bottom-menu">
                            
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="copyright">
                                <p>© <?php echo date('Y') ?> Copyright ihracat.in</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<button id="gotopbtn" title="Go to top" style="display: block;"><i class="fa fa-angle-up" aria-hidden="true"></i>
    <b>
        <div class="flame"></div>
    </b>
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">


  <div class="modal-dialog modal-dialog-centered  modal-lg width-900">
  <!----><div class="modal-logo-section">
        <img src="assets/images/logo.png" alt="" />
    </div>
    <div class="modal-footer-section"><p>© Copyright <?php echo date('Y') ?> . ihracat.in </p></div>
    <div class="modal-content p-5">

      <div class="modal-body  ">

      <section class="contact-section p-0 bg-transparent"> 
        <div class="contact-form">
            <h3><?php echo $lang['application_form']; ?></h3>
            <p><?php echo $lang['application_form_content']; ?></p>
            
            <form action="export_authority.php" method="POST" id="quick_application">
                <div class="row">
                    <div class="col-md-12"><div class="blue-line"></div></div>
                    <div class="col-md-7">
                        <div class="info-section-title">
                            <div class="title"><?php echo $lang['personal_information']; ?></div>
                            <div class="line"></div>
                        </div>
                    </div>
                    <div class="col-md-5"></div>
                    <div class="col-md-6"><input type="text" name="fullname" required placeholder="<?php echo $lang['contact_name_surname'] ?>"/></div>
                    <div class="col-md-6"><input type="email" name="email" required  placeholder="<?php echo $lang['contact_email'] ?>"/></div>
                    <div class="col-md-6"><input type="text" name="contact_title"  placeholder="<?php echo $lang['contact_title'] ?>"/></div>
                    <div class="col-md-6"><input type="phone" name="phone" id="phone"  placeholder="<?php echo $lang['contact_phone_number'] ?>"/></div>

                    <div class="col-md-7">
                        <div class="info-section-title">
                            <div class="title"> <?php echo $lang['company_information']; ?></div>
                            <div class="line"></div>
                        </div>
                    </div>
                    <div class="col-md-5"></div>
                    <div class="col-md-6"><input type="text" name="contact_company_title"  placeholder="<?php echo $lang['contact_company_title'] ?>"/></div>
                    <div class="col-md-6"><input type="text" name="export_authority"  placeholder="<?php echo $lang['export_authority'] ?>"/></div>
                    <div class="col-md-12">
                        <div class="contact-btn apply-form">
                        <input type="submit" class="applyformbtntwo"  value="<?php echo $lang['send_form'] ?>" />
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $lang['cancel'] ?></button>
                         
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
      </div>
    </div>
  </div>
</div>





<!-- Modal -->
<div class="modal fade" id="exampleModaltwo" tabindex="-1" aria-labelledby="exampleModaltwoLabel" aria-hidden="true">


  <div class="modal-dialog modal-dialog-centered  modal-lg width-900">
  <!----><div class="modal-logo-section">
        <img src="assets/images/logo.png" alt="" />
    </div>
    <div class="modal-footer-section"><p>© Copyright <?php echo date('Y') ?> . ihracat.in </p></div>
    <div class="modal-content p-5">

      <div class="modal-body  ">

      <section class="contact-section p-0 bg-transparent"> 
        <div class="contact-form">
            <h3><?php echo $lang['application_form']; ?></h3>
            <p><?php echo $lang['application_form_content']; ?></p>
            
            <form action="export_authority.php" method="POST" id="sendproductname">
                <div class="row">
                    <div class="col-md-2"> <div class="blue-line"></div></div>
                    <div class="col-md-10">
                        <div class="product-name-title">
                            <div class="title"  ><?php echo $lang['productname_or_code'] ?>:</div>
                            <div class="product-section"></div>
                            <input type="text"  class="content" id="producttext"  name="productname" value="deneme" />
                        </div>
                    </div>
                   
                    <div class="col-md-7">
                        <div class="info-section-title">
                            <div class="title"><?php echo $lang['personal_information']; ?></div>
                            <div class="line"></div>
                        </div>
                    </div>
                    <div class="col-md-5"></div>
                    <div class="col-md-6"><input type="text" name="fullname" required placeholder="<?php echo $lang['contact_name_surname'] ?>"/></div>
                    <div class="col-md-6"><input type="email" name="email" required  placeholder="<?php echo $lang['contact_email'] ?>"/></div>
                    <div class="col-md-6"><input type="text" name="contact_title"  placeholder="<?php echo $lang['contact_title'] ?>"/></div>
                    <div class="col-md-6"><input type="phone" name="phone" id="productphone"  placeholder="<?php echo $lang['contact_phone_number'] ?>"/></div>

                    <div class="col-md-7">
                        <div class="info-section-title">
                            <div class="title"> <?php echo $lang['company_information']; ?></div>
                            <div class="line"></div>
                        </div>
                    </div>
                    <div class="col-md-5"></div>
                    <div class="col-md-6"><input type="text" name="contact_company_title"  placeholder="<?php echo $lang['contact_company_title'] ?>"/></div>
                    <div class="col-md-6"><input type="text" name="export_authority"  placeholder="<?php echo $lang['export_authority'] ?>"/></div>
                    <div class="col-md-12">
                        <div class="contact-btn apply-form">
                        <input type="submit" class="applyformbtnone"  value="<?php echo $lang['send_form'] ?>" />
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $lang['cancel'] ?></button>
                         
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
      </div>
    </div>
  </div>
</div>