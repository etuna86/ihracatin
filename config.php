<?php
session_start();
require __DIR__ . '/vendor/autoload.php';

$config = [
  'baseDirectory' => '',
  'smtp' => [
    'name' => 'İhracatin Web Site',
    'email' => 'digitalexchangesender@gmail.com',
    'pass' => 'send2018digital',
    'addAdrress' => 'info@digitalexchange.com.tr'
    //'addAdrress' => 'erol.tuna@digitalexchange.com.tr'
  ]
];

if($_SERVER['SERVER_NAME'] == 'localhost'){
  $config['baseDirectory'] = '/ihracatin';
}

if (!isset($_SESSION["lang"])){
  require("lang/tr.php");
}else {
  require("lang/".$_SESSION["lang"].".php");
}

function url($filename = '') {
  global $config;

  if($filename != ''){
    $filename = '/' . $filename;
  }

  echo "https://" . $_SERVER['SERVER_NAME'] . $config['baseDirectory'] . $filename;
}

function postFilter(&$value) {
  $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
}
