<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $lang['partners']; ?> - <?php echo $lang['ihracatin']; ?></title>
    <meta name="description" content="<?php echo $lang['metadesc_partners'] ?>" />
    <?php include 'includes/styles.php'; ?>
</head>
<body>
<?php include 'includes/header.php'; ?>
<section class="pageheader">
    <div class="page-header-content">
        <div class="page-header-content-box">
            <div class="container">
            <h1><?php echo $lang['partners_pageheader_title']; ?></h1>
                <div class="page-header-menu">
                    <ul>
                        <li><a href="index.php"><?php echo $lang['mainpage']; ?>&nbsp;-&nbsp;</a></li>
                        <li><a class="active"><?php echo $lang['partners']; ?>&nbsp;</a></li>
                    </ul>
                </div>
               
            </div>
        </div>
        <div class="bottom"></div>
    </div>
    <img src="assets/images/pageheaders/collaborators.jpg" alt="" />

</section>
<section class="main-content">
    <div class="container">
        <div class="team-section partners">
            <div class="homeboxleft">
                <img src="assets/images/homepages/homeboxleft.png" />
            </div>
            <div class="row">
                <div class="partners-title">
                    <h2 ><?php echo $lang['technology_partners']; ?></h2>
                    <div class="blue-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <a href="https://google.com/" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/technology/google.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://google.com/" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/technology/google-partner.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://google.com/" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/technology/google_market_finder.jpg" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://facebook.com/" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/technology/fb.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://facebook.com/" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/technology/fb-marketing.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://iabtr.org/" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/technology/iab.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://mmaturkey.org/" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/technology/mma.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://gemius.com/" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/technology/gemius.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://adespresso.com/" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/technology/adespresso.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://supermetrics.com/" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/technology/supermetrics.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://theadx.com/" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/technology/theadx2.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://similarweb.com/" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/technology/similarweb.png" alt="" /> 
                        </div>
                    </a>
                </div>

            </div>
            <div class="row">
                <div class="partners-title">
                    <h2 ><?php echo $lang['bussines_partners']; ?></h2>
                    <div class="blue-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <a href="https://ideasoft.com.tr" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/business/ideasoft.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://kantarmedia.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/business/kantar.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://revotas.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/business/revotas.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://useinsider.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/business/insider.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://alloka.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/business/alloka.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://emyd.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/business/emyd.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://www.tsoft.com.tr" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/business/tsoft.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://www.inveon.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/business/inveon.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://www.shopify.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/business/shopify.jpg" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://www.jivochat.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/business/jivochat.jpg" alt="" /> 
                        </div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="partners-title">
                    <h2 ><?php echo $lang['data_partners']; ?></h2>
                    <div class="blue-line"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <a href="https://www.accumulatedata.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/partners/data/accumulate.png" alt="" /> 
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'includes/footer.php'; ?>

<?php include 'includes/scripts.php'; ?>
</body>
</html>
