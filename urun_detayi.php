<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ihracat.in</title>
    <?php include 'includes/styles.php'; ?>
</head>
<body>
<?php include 'includes/header.php'; ?>
<section class="pageheader">
    <div class="page-header-content">
        <div class="page-header-content-box">
            <div class="container">
                <h3>Ürün Detayı</h3>
                <div class="page-header-menu">
                    <ul>
                        <li><a href="index.php">Ana Sayfa&nbsp;-&nbsp;</a></li>
                        <li><a>Ürünler&nbsp;-&nbsp;</a></li>
                        <li><a  class="active">Ürün Detayı</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bottom"></div>
    </div>
    <img src="assets/images/pageheaders/urun_detay-pageheader.jpg" alt="" />

</section>
<section class="main-content ">
    <div class="tab-box p-0">
            <div class="container">
                <div class="tab-box-inside">
                    <div class="row">
                    <div class="col-md-6">
                            <div class="tab-box-img left-img">
                                <div class="img-line">
                                    <div class="left"></div>
                                    <div class="right"></div>
                                </div>
                                <div class="dots-left">
                                    <img src="assets/images/homepages/homeboxleft.png" alt="">
                                </div>
                                <img id="left50-img" src="assets/images/exporters.jpg" alt="exporters" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="home-exporters-box">
                                <div class="home-exporters-box-inside">
                                    <h3>NEDİR NE İŞE YARAR ?</h3>
                                    <h2>web.tv Canlı Yayın çevrimiçi etkinliklerimizi üst düzeyde taşıyın.</h2>
                                    <p>İster global yayınlar, isterse yüksek profilli pazarlama ve eğitim etkinlikleri için profesyonel canlı yayın sistemi sunuyor, binlerce kişiye kolayca ulaşmanızı sağlıyoruz. </p>
                                    <p>
                                    Webinar oluşturmak, sunum gerçekleştirmek, izleyicileri canlı yayınına dahil ederek webinar esnasında etkileşim kurmak ve daha bir çok etkileyici özellik ve çevrimiçi etkinliklerinizi kolaylaştırıyor, efektif hale getiriyoruz. 
                                    </p>
                                    <div class="try-now">
                                        <a href="#">30 dk. Hemen Dene</a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                    </div>
                    

                </div>
                
            </div>
        </div>
    </div>
</section>
<div class="product-img">
    <img src="assets/images/products/product_img.jpg" alt="" />
</div>


<section class="main-options">
    <div class="left-absolute"></div>
    <div class="tab-box p-0">
        <div class="container">
            <div class="tab-box-inside">
                <div class="row">
                    <div class="col-md-6">
                        <div class="home-exporters-box">
                            <div class="home-exporters-box-inside">
                                <h3>ANA ÖZELLİKLER</h3>
                                <h2>Çevrimiçi etkinlikler için ihtiyacınız olan her şey</h2>
                                <p>Size özel “kanaladim.web.tv” linki ile geçekleştirin, marka bilinci oluşturun ve koruyun. İlgi çekici, etkileşimli bir kullanıcı deneyimi sunan, marka bilincini koruyan canlı yayınlar üretin.</p>
                                <p>
                                Pazarlama ve eğitim süreçlerini modernize edin. Güçlü canlı yayın altyapısı ile kullanımı kolay, güvenli ve ölçeklenebilir ortamda geniş kitlelere hızlı ve kolayca erişin. 
                                </p>
                                <div class="try-now">
                                    <a href="#">30 dk. Hemen Dene</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="tab-rules">
                            <div class="rules-box">
                                <div class="rules-icon">
                                    <i class="fa fa-globe-americas" aria-hidden="true"></i>
                                </div>
                                <div class="rules-desc">
                                    <p>
                                    Katılımcıların canlı Soru - cevap, yoklama ve sosyal medya yayınları aracılığıyla sunum yapan kişilere etkileşime girmesini sağlayarak katılımı artırın. 
                                    </p>
                                </div>
                            </div>
                            <div class="rules-box">
                                <div class="rules-icon">
                                    <i class="fa fa-globe-americas" aria-hidden="true"></i>
                                </div>
                                <div class="rules-desc">
                                    <p>
                                    Katılımcıların canlı Soru - cevap, yoklama ve sosyal medya yayınları aracılığıyla sunum yapan kişilere etkileşime girmesini sağlayarak katılımı artırın. 
                                    </p>
                                </div>
                            </div>
                            <div class="rules-box">
                                <div class="rules-icon">
                                    <i class="fa fa-globe-americas" aria-hidden="true"></i>
                                </div>
                                <div class="rules-desc">
                                    <p>
                                    Katılımcıların canlı Soru - cevap, yoklama ve sosyal medya yayınları aracılığıyla sunum yapan kişilere etkileşime girmesini sağlayarak katılımı artırın. 
                                    </p>
                                </div>
                            </div>
                            <div class="rules-box">
                                <div class="rules-icon">
                                    <i class="fa fa-globe-americas" aria-hidden="true"></i>
                                </div>
                                <div class="rules-desc">
                                    <p>
                                    Katılımcıların canlı Soru - cevap, yoklama ve sosyal medya yayınları aracılığıyla sunum yapan kişilere etkileşime girmesini sağlayarak katılımı artırın. 
                                    </p>
                                </div>
                            </div>
                            <div class="rules-box">
                                <div class="rules-icon">
                                    <i class="fa fa-globe-americas" aria-hidden="true"></i>
                                </div>
                                <div class="rules-desc">
                                    <p>
                                    Katılımcıların canlı Soru - cevap, yoklama ve sosyal medya yayınları aracılığıyla sunum yapan kişilere etkileşime girmesini sağlayarak katılımı artırın. 
                                    </p>
                                </div>
                            </div>
                            <div class="rules-box">
                                <div class="rules-icon">
                                    <i class="fa fa-globe-americas" aria-hidden="true"></i>
                                </div>
                                <div class="rules-desc">
                                    <p>
                                    Katılımcıların canlı Soru - cevap, yoklama ve sosyal medya yayınları aracılığıyla sunum yapan kişilere etkileşime girmesini sağlayarak katılımı artırın. 
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>  
            </div>
        </div>
    </div>  
</section>

<section class="additional-features-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2>EK ÖZELLİKLERİ</h2>
                <div class="additional-features">
                  
                    <div class="features-box">
                        <div class="features-img">
                            <img src="assets/images/additional-features/files.png" alt="" />
                        </div>
                        <h3>Kullanımı Kolay, <br> Web Tabanlı Bağlantı !</h3>
                        <p>İndirme veya yükleme gerekmez. Online Danışmanlık ofisinize dizüstü bilgisayarınızdan, tabletinizden veya cep telefonunuzdan bağlanın.</p>
                    </div>
                    <div class="features-box">
                        <div class="features-img">
                            <img src="assets/images/additional-features/video-call.png" alt="" />
                        </div>
                        <h3>Yüz Yüze Konuşun</h3>
                        <p>Video konferans aracı ile danışanlarınızla birebir etkileşim kurarak uzmanı olduğunuz alan için online danışmanlık hizmeti sunun. </p>
                    </div>
                    <div class="features-box">
                        <div class="features-img">
                            <img src="assets/images/additional-features/live-streaming.png" alt="" />
                        </div>
                        <h3>Ekran Paylaşımı </h3>
                        <p>Ekran paylaşımı özelliği ile danışanlarınız ile kolayca evrak / dosya paylaşın.</p>
                    </div>
                    <div class="features-box">
                        <div class="features-img">
                            <img src="assets/images/additional-features/payment.png" alt="" />
                        </div>
                        <h3>Online Ödeme Alt Yapısı</h3>
                        <p>İsterseniz online vereceğiniz hizmetlerinizin bedellerini de ödeme alt yapısı özelliği ile hizmet öncesi online tahsil edin.</p>
                    </div>
                    <div class="features-box">
                        <div class="features-img">
                            <img src="assets/images/additional-features/privacy.png" alt="" />
                        </div>
                        <h3>Güvenlik</h3>
                        <p>Web.tv en güvenli güvenlik ve gizlilik sertifikalarına sahip türünün en iyisi veri merkezlerini kullanır. </p>
                    </div>
                    <div class="features-box">
                        <div class="features-img">
                            <img src="assets/images/additional-features/online-learning.png" alt="" />
                        </div>
                        <h3>İş Devamlılığı </h3>
                        <p>Bir kriz veya felaket durumunda iletişiminizi, hizmetlerinizi aksatmayın, işinizin devamlılığını sağlayın.</p>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>

<?php include 'includes/footer.php'; ?>

<?php include 'includes/scripts.php'; ?>
</body>
</html>
