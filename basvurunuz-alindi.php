<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $lang['ihracatin']; ?></title>
    <meta name="description" content="" />
    <?php include 'includes/styles.php'; ?>
</head>
<body>
<?php include 'includes/header.php'; ?>

<div class="apply-page">
    <div class="apply-desc">
        <div class="container">
            <div class="alert alert-info" role="alert">
                <strong><?php if(isset($_GET['response'])){ echo $_GET['response']; } ?></strong>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footer.php'; ?>

<?php include 'includes/scripts.php'; ?>
</body>
</html>
