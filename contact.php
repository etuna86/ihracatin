<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $lang['contact']; ?> - <?php echo $lang['ihracatin']; ?></title>
    <meta name="description" content="<?php echo $lang['metadesc_contact'] ?>" />
    <?php include 'includes/styles.php'; ?>
</head>
<body>
<?php include 'includes/header.php'; ?>
<section class="pageheader">
    <div class="page-header-content">
        <div class="page-header-content-box">
            <div class="container">
                <h1><?php echo $lang['contact']; ?></h1>
                <div class="page-header-menu">
                    <ul>
                        <li><a href="index.php"><?php echo $lang['mainpage']; ?>&nbsp;-&nbsp;</a></li>
                        <li><a  class="active"><?php echo $lang['contact']; ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bottom"></div>
    </div>
    <img src="assets/images/pageheaders/contact.jpg" alt="" />

</section>

<section class="contact-section"> 
    <div class="contact-left">
        <img src="assets/images/homepages/homeboxleft.png" alt="" />
    </div>
    <div class="contact-right">
        <img src="assets/images/Combined-Shape.png" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="contact-info">
                    <h3><?php echo $lang['contact_info']; ?></h3>
                    <label for=""> <?php echo $lang['contact_address']; ?></label>
                    <div class="contact-box">
                        <div class="contact-icon">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </div>
                        <div class="contact-desc">
                            <p>Eski Büyükdere Caddesi Büyükdere İş merkezi Kat: 6 4.Levent İstanbul </p>
                        </div>
                    </div>
                    <label for=""><?php echo $lang['contact_phone']; ?></label>
                    <div class="contact-box">
                        <div class="contact-icon">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                        </div>
                        <div class="contact-desc">
                           <a href="tel:0212 809 77 37">0212 809 77 37</a>
                        </div>
                    </div>
                    <label for=""><?php echo $lang['email']; ?></label>
                    <div class="contact-box">
                        <div class="contact-icon">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </div>
                        <div class="contact-desc">
                            <a href="mailto:info@ihracat.in">info@ihracat.in</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="contact-form">
                    <h3><?php echo $lang['contact_form']; ?></h3>
                    <p><?php echo $lang['contact_form_desc']; ?></p>
                    <div class="blue-line"></div>
                    <form action="contact_action.php" method="POST" id="contact_form"  >
                        <div class="row">
                            <div class="col-md-6"><input type="text" name="fullname"  placeholder="<?php echo $lang['contact_name_surname']; ?>"/></div>
                            <div class="col-md-6"><input type="email" name="email"  placeholder="<?php echo $lang['contact_email']; ?>"/></div>
                            <div class="col-md-6"><input type="text"  name="company_title"  placeholder="<?php echo $lang['contact_company']; ?>"/></div>
                            <div class="col-md-6"><input type="phone" id="contact_phone" name="phone"  placeholder="<?php echo $lang['contact_phone']; ?>"/></div>
                            <div class="col-md-12"><textarea name="message" name=""  id="" cols="1" rows="1" placeholder="<?php echo $lang['message']; ?>"></textarea></div>
                            <div class="col-md-12">
                                <div class="contact-btn">
                                    <input type="submit" class="applyformbtnthree"  value="<?php echo $lang['send_form']; ?>" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="map-section">
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d5057.226014060065!2d29.001005615784646!3d41.08987260846145!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe579875beca32b68!2zQsOcWcOcS0RFUkUgxLDFniBNRVJLRVpJ!5e0!3m2!1str!2str!4v1600413445730!5m2!1str!2str" width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
<?php include 'includes/footer.php'; ?>

<?php include 'includes/scripts.php'; ?>
</body>
</html>
