<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $lang['mainpage'] ?> - <?php echo $lang['ihracatin'] ?></title>
    <meta name="description" content="<?php echo $lang['metadesc_mainpage'] ?>" />
    <?php include 'includes/styles.php'; ?>
</head>
<body>
<?php include 'includes/header.php'; ?>
<section class="main-pageheader">
    <div class="pageheader-main-section">
        <div class="container">
            <div class="mainpage-header-section">
                <div class="main-pageheader-left">
                    <div class="pageheader-box">
                        <h1><?php echo $lang['mainpage_pageheader_title']; ?> </h1>
                        <p><?php echo $lang['mainpage_pageheader_desc']; ?></p>
                    </div>
                </div>
                <div class="main-pageheader-right">
                    <div class="pageheader-box">
                        <form action="#" onsubmit="return: false;" >
                            <input type="text"  id="productinput"  placeholder="<?php echo $lang['mainpage_product_name']; ?>" />
                            <input type="button"  id="findbuyer" class="quickapplytwo"  data-toggle="modal" data-target="#exampleModaltwo" disabled="true"  value="<?php echo $lang['quick_application']; ?>" />
                        </form>
                        <p>
                            <?php echo $lang['mainpage_new_normal_desc']; ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img src="assets/images/pageheaders/home-pageheader.jpg" alt="" />
</section>

<section class="exporter-solutions-section">
    <div class="container">
        <div class="exporter-solutions">
            <h1> <?php echo $lang['mainpage_exporter_solutions_title']; ?> </h1>
                <p><?php echo $lang['mainpage_exporter_solutions_desc_one']; ?></p> 
            <div class="quick-aplication-btn">
                <a href="#" data-toggle="modal" class="quickapplythree"  data-target="#exampleModal" >
                     <?php echo $lang['quick_application']; ?>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="home-exporter-section " id="exporters" >
    <div class="nav-tabs-header" id="exporters2">
    <nav id="exporters3">
        <div class="nav nav-tabs" id="nav-tab" role="tablist"  >
            <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true"><?php echo $lang['exporters']; ?></a>
            <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"> <?php echo $lang['thinking_export']; ?> </a>
            <a class="nav-link" id="eexporter-tab" data-toggle="tab" href="#eexporter" role="tab" aria-controls="eexporter" aria-selected="false"><?php echo $lang['e_export']; ?></a>
        </div>
    </nav>
    </div>
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
        <div class="tab-box" >
            <div class="container">
                <div class="tab-box-inside">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="home-exporters-box">
                                <div class="home-exporters-box-inside">
                                    <h3><?php echo $lang['big_exporters']; ?></h3>
                                    <h2><?php echo $lang['mainpage_exporters_title']; ?></h2>
                                    <p><?php echo $lang['mainpage_exporters_desc']; ?></p>
                                <div class="blue-line"></div>
                               
                               
                                </div>
                              
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="tab-box-img">
                            <div class="img-line">
                                <div class="left"></div>
                                <div class="right"></div>
                            </div>
                                <img id="right50-img" src="assets/images/homepages/tab-1.jpg" alt="exporters">
                            </div>
                        </div>
                    </div>
                    

                </div>
                
            </div>
        </div>
    </div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
  <div class="tab-box" >
            <div class="container">
                <div class="tab-box-inside">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="home-exporters-box">
                                <div class="home-exporters-box-inside">
                                    <h3><?php echo $lang['big_thinking_export']; ?></h3>
                                    <h2><?php echo $lang['big_thinking_export_title']; ?></h2>
                                    <p><?php  echo $lang['big_thinking_export_desc']; ?> </p>
                                </div>
                              
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="tab-box-img">
                            <div class="img-line">
                                <div class="left"></div>
                                <div class="right"></div>
                            </div>
                                <img id="right50-imgtwo" src="assets/images/homepages/tab-2.jpg" alt="exporters">
                            </div>
                        </div>
                    </div>
                    

                </div>
                
            </div>
        </div>
  </div>
  <div class="tab-pane fade" id="eexporter" role="tabpanel" aria-labelledby="eexporter-tab">
  <div class="tab-box" >
            <div class="container">
                <div class="tab-box-inside">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="home-exporters-box">
                                <div class="home-exporters-box-inside">
                                    <h3> <?php echo $lang['big_e_export']; ?></h3>
                                    <h2><?php echo $lang['big_e_export_title']; ?></h2>
                                    <p><?php echo $lang['big_e_export_desc']; ?></p>
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="tab-box-img">
                            <div class="img-line">
                                <div class="left"></div>
                                <div class="right"></div>
                            </div>
                                <img id="right50-imgthree" src="assets/images/homepages/tab-3.jpg" alt="exporters">
                            </div>
                        </div>
                    </div>
                    

                </div>
                
            </div>
        </div>
  </div>
</div>
</section>

<section class="homepage-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="homeboxleft">
                    <img src="assets/images/homepages/homeboxleft.png" />
                </div>
                <div class="home-box">
                  
                    <div class="home-box-img">
                      <div class="box-img one"></div>
                    </div>
                    <h3><?php echo $lang['mainpage_es_one_title']; ?></h3>
                    <p><?php echo $lang['mainpage_es_one_desc']; ?>
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="home-box" id="customerdata">
                    <div class="home-box-img">
                    <div class="box-img two"></div>
                    </div>
                    <h3><?php echo $lang['mainpage_es_two_title']; ?></h3>
                    <p>
                    <?php echo $lang['mainpage_es_two_desc']; ?>
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="home-box" id="marketresearch">
                    <div class="home-box-img">
                        <div class="box-img three"></div>
                    </div>
                    <h3><?php echo $lang['mainpage_es_three_title']; ?></h3>
                    <p><?php echo $lang['mainpage_es_three_desc']; ?>
                    </p>
                </div>
            </div>
            <div class="col-md-6">
            <div class="homeboxright">
                    <img src="assets/images/homepages/homeboxleft.png" />
                </div>
                <div class="home-box" id="datamining">
                    <div class="home-box-img">
                        <div class="box-img four"></div>
                    </div>
                    <h3><?php echo $lang['mainpage_es_four_title']; ?></h3>
                    <p>
                    <?php echo $lang['mainpage_es_four_desc']; ?>
                    </p>
                </div>
              
            </div>
            <div class="col-md-6">
                <div class="home-box" id="createbrand">
                    <div class="home-box-img">
                    <div class="box-img five"></div>
                    </div>
                    <h3><?php echo $lang['mainpage_es_five_title']; ?></h3>
                    <p>
                    <?php echo $lang['mainpage_es_five_desc']; ?>
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="home-box">
                    <div class="home-box-img">
                    <div class="box-img six"></div>
                    </div>
                    <h3><?php echo $lang['mainpage_es_six_title']; ?></h3>
                    <p>
                        <?php echo $lang['mainpage_es_six_desc']; ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>




<?php include 'includes/footer.php'; ?>

<?php include 'includes/scripts.php'; ?>
</body>
</html>
