<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $lang['export_consultants']; ?> - <?php echo $lang['ihracatin']; ?></title>
    <meta name="description" content="<?php echo $lang['metadesc_export_consultant'] ?>" />
    <?php include 'includes/styles.php'; ?>
</head>
<body>
<?php include 'includes/header.php'; ?>
<section class="pageheader">
    <div class="page-header-content">
        <div class="page-header-content-box">
            <div class="container">
            <h1><?php echo $lang['team_pageheader_title']; ?></h1>
                <div class="page-header-menu">
                    <ul>
                        <li><a href="index.php"><?php echo $lang['mainpage']; ?>&nbsp;-&nbsp;</a></li>
                        <li><a class="active"><?php echo $lang['team']; ?>&nbsp;</a></li>
                    </ul>
                </div>
               
            </div>
        </div>
        <div class="bottom"></div>
    </div>
    <img src="assets/images/pageheaders/export_consultants.jpg" alt="" />

</section>
<section class="main-content">
    <div class="container">
        <div class="team-section team-members">
        <div class="homeboxleft">
        <img src="assets/images/homepages/homeboxleft.png" />
        </div>
            <h2><?php echo $lang['team_synergy']; ?></h2>
            <div class="row">
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/cofounder.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_one']; ?></h3>
                        <h4><?php echo $lang['team_member_title_one']; ?></h4>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                        <div class="team-img">
                            <img src="assets/images/team/handan.jpg" alt="" />
                        </div>
                        <h3><?php echo $lang['team_member_name_two']; ?></h3>
                        <h4><?php echo $lang['team_member_title_two']; ?></h4>
                        <p><?php echo $lang['team_membertwo_desc_one']; ?></p>
                        <p><?php echo $lang['team_membertwo_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/rinet.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_three']; ?></h3>
                        <h4><?php echo $lang['team_member_title_three']; ?></h4>
                        <p><?php echo $lang['team_memberthree_desc_one']; ?></p>
                        <p><?php echo $lang['team_memberthree_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/baris.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_four']; ?></h3>
                        <h4><?php echo $lang['team_member_title_four']; ?></h4>
                        <p><?php echo $lang['team_memberfour_desc_one']; ?></p>
                        <p><?php echo $lang['team_memberfour_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/ozan.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_five']; ?></h3>
                        <h4><?php echo $lang['team_member_title_five']; ?></h4>
                        <p><?php echo $lang['team_memberfive_desc_one']; ?></p>
                        <p><?php echo $lang['team_memberfive_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/dilara.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_six']; ?></h3>
                        <h4><?php echo $lang['team_member_title_six']; ?></h4>
                        <p><?php echo $lang['team_membersix_desc_one']; ?></p>
                        <p><?php echo $lang['team_membersix_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/ceren.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_seven']; ?></h3>
                        <h4><?php echo $lang['team_member_title_seven']; ?></h4>
                        <p><?php echo $lang['team_memberseven_desc_one']; ?></p>
                        <p><?php echo $lang['team_memberseven_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/erman.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_eight']; ?></h3>
                        <h4><?php echo $lang['team_member_title_eight']; ?></h4>
                        <p><?php echo $lang['team_membereight_desc_one']; ?></p>
                        <p><?php echo $lang['team_membereight_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/serra.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_nine']; ?></h3>
                        <h4><?php echo $lang['team_member_title_nine']; ?></h4>
                        <p><?php echo $lang['team_membernine_desc_one']; ?></p>
                        <p><?php echo $lang['team_membernine_desc_two']; ?></p>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/misra.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_ten']; ?></h3>
                        <h4><?php echo $lang['team_member_title_ten']; ?></h4>
                        <p><?php echo $lang['team_memberten_desc_one']; ?></p>
                        <p><?php echo $lang['team_memberten_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/dilge.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_eleven']; ?></h3>
                        <h4><?php echo $lang['team_member_title_eleven']; ?></h4>
                        <p><?php echo $lang['team_membereleven_desc_one']; ?></p>
                        <p><?php echo $lang['team_membereleven_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/kami.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_twelve']; ?></h3>
                        <h4><?php echo $lang['team_member_title_twelve']; ?></h4>
                        <p><?php echo $lang['team_membertwelve_desc_one']; ?></p>
                        <p><?php echo $lang['team_membertwelve_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/efe.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_thirteen']; ?></h3>
                        <h4><?php echo $lang['team_member_title_thirteen']; ?></h4>
                        <p><?php echo $lang['team_memberthirteen_desc_one']; ?></p>
                        <p><?php echo $lang['team_memberthirteen_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/merve.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_fourteen']; ?></h3>
                        <h4><?php echo $lang['team_member_title_fourteen']; ?></h4>
                        <p><?php echo $lang['team_memberfourteen_desc_one']; ?></p>
                        <p><?php echo $lang['team_memberfourteen_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/ugur.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_fifteen']; ?></h3>
                        <h4><?php echo $lang['team_member_title_fifteen']; ?></h4>
                        <p><?php echo $lang['team_memberfifteen_desc_one']; ?></p>
                        <p><?php echo $lang['team_memberfifteen_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/erol.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_sixteen']; ?></h3>
                        <h4><?php echo $lang['team_member_title_sixteen']; ?></h4>
                        <p><?php echo $lang['team_membersixteen_desc_one']; ?></p>
                        <p><?php echo $lang['team_membersixteen_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/efecan.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_seventeen']; ?></h3>
                        <h4><?php echo $lang['team_member_title_seventeen']; ?></h4>
                        <p><?php echo $lang['team_memberseventeen_desc_one']; ?></p>
                        <p><?php echo $lang['team_memberseventeen_desc_two']; ?></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="team-box">
                    <div class="team-img">
                        <img src="assets/images/team/bahadir.jpg" alt="" />
                    </div>
                        <h3><?php echo $lang['team_member_name_eighteen']; ?></h3>
                        <h4><?php echo $lang['team_member_title_eighteen']; ?></h4>
                        <p><?php echo $lang['team_membereighteen_desc_one']; ?></p>
                        <p><?php echo $lang['team_membereighteen_desc_two']; ?></p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<?php include 'includes/footer.php'; ?>

<?php include 'includes/scripts.php'; ?>
</body>
</html>
