<?php
include 'config.php';

header('Content-Type: application/json; charset=utf-8');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

array_walk_recursive($_POST, "postFilter");

$output = [
  'error' => false,
  'info' => ''
];

$productname='';

if(mb_strlen($_POST['fullname']) <= 3) {
  $output['error'] = true;
  $output['info'] = $lang['write_name'];
}

if (! filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
  $output['error'] = true;
  $output['info'] = $lang['invalid_email'];
}

if(! isset($_POST['message']) || mb_strlen($_POST['message']) <= 0){
  $_POST['message'] =  $lang['message_notfound'];
}

if(isset($_POST['productname']) || mb_strlen($_POST['productname']) >= 0){
  $productname= $_POST['productname']. '<br>';
}

if($output['error'] === false) {
  $mail = new PHPMailer();
  $mail->IsSMTP();
  $mail->CharSet = 'UTF-8';
  $mail->Mailer = "smtp";
  $mail->SMTPDebug  = 0;  
  $mail->SMTPAuth   = TRUE;
  $mail->SMTPSecure = "tls";
  $mail->Port       = 587;
  $mail->Host       = "smtp.gmail.com";
  $mail->Username   = $config['smtp']['email'];
  $mail->Password   = $config['smtp']['pass'];
  $mail->IsHTML(true);
  $mail->SetFrom($config['smtp']['email'], $config['smtp']['name']);
  $mail->AddAddress($config['smtp']['addAdrress']);

  $mail->Subject = $_POST['fullname'] . " | via Quick Application Form";
  $content = $productname . $_POST['fullname'] . '<br>' . $_POST['email'] . '<br>' . $_POST['contact_title'] .'<br>' . $_POST['phone'] . '<br>' . $_POST['contact_company_title'] .'<br>' . $_POST['export_authority'];

  $mail->MsgHTML($content);

  if(!$mail->Send()) {
    $output['error'] = true;
    $output['info'] = $lang['contact_error_message']." ".$lang['please_email'];
  } else {
    $output['info'] = $lang['received_your_info']." ".$lang['get_back_soon'];
  }
}


echo json_encode($output);
