<?php
$lang=array(
    'ihracatin' => 'ihracat.in',

    
    /*metadescription  start*/
    'metadesc_mainpage' => 'You can take a look at our exporter solutions from our homepage to develop your business and open up to new markets.',
    'metadesc_google_market_finder' => 'Promote your products to new customers from all over the world and expand into new markets with Google Marketfinder.',
    'metadesc_theadx_export' => 'Examine the export data of the whole world in detail with TheADX Export, an artificial intelligence-based export planning tool.',
    'metadesc_together_social' => 'With 2gether Social, promote your products with well-known people on social media and effective marketing techniques without creating an advertising perception.',
    'metadesc_media_planning' => 'Identify your potential customers globally by promoting your brand in target markets with special and powerful media planning.',
    'metadesc_online_sales_site' => 'Sell ​​your products around the world with an online sales site that is localized and equipped according to your target market.',
    'metadesc_dmp' => 'Get to know your visitors with Data Management (DMP), create detailed segments based on their behavior on your website.',
    'metadesc_web_mobil_app' => 'Develop your SEO compatible web and mobile application to convert your visitors into customers with web mobile application development.',
    'metadesc_emarketing' => 'With e-marketplace management, add  products to global marketplaces to develop your e-exports while adding them to local marketplaces.',
    'metadesc_socialmedia_export' => 'With Social Media Export, take your company beyond the borders and get new customers from all over the world.',
    'metadesc_export_consultant' => 'Your product or service succeeds with the team synergy of our export consultants.',
    'metadesc_referances' => 'You can take a look at our customers working with us from our references page.',
    'metadesc_partners' => 'You can take a look at our technology partners on our partner page.',
    'metadesc_contact' => 'learn our contact information from the contact page, or you can contact us by filling out the contact form.
    ',
    
    /*metadescription finish*/
    /*header  start */
 
    'mainpage' => 'Home',
    'exporter_solution' => ' Exporter Solutions',
    'exporters' => 'Exporters',
    'thinking_export' => 'Those who intend to export',
    'e_export' => 'E-export',
    'products' => 'Products',
    'google_marketfinder' => 'Google Market Finder',
    //'theadx_export' => 'TheADX Export',
    'theadx_export' => 'Export Management with Artificial Intelligence',
    //'together_social' => '2gether Social',
    'together_social' => '2gether Social - Influencer and Content Delivery Network',
    'media_planning' => 'Media Planning',
    'online_sale_site' => 'Online Sales Site',
    'data_management' => 'Data Management (DMP)',
    'web_mobil_application_develop' => 'Web Mobile Application Development',
    'emarketing_management' => 'E-Marketplace Management',
    'facebook_export' => 'Social Media Export',
    //'we_doing' => 'What are we doing?',
    'we_doing' => 'Things We Do',
    'market_research' => 'The market research',
    'planning_with_ai' => 'Marketing with AI',
    'customer_data' => 'Customer Data',
    'data_mining' => 'Data mining',
    'create_brand' => 'Brand Building',
    'export_consultants' => 'Export Consultants',
    'collaborators' => 'Referances',
    'partners' => 'Partners',
    'government_supports' => 'Government Supports',
    'contact' => 'Contact',
    /*header  finish */

  /*mainpage  start */
  'mainpage_pageheader_title' => 'How to Export in the New Normal?',
  'mainpage_pageheader_desc' => 'With the world’s first AI-based export analysis and customer match tool, develop your business and find new marketplaces, start to export, or grow your business..
  ',
  'mainpage_product_name'=>'Write the name of your product or your code', 
  'mainpage_find_buyer'=>'Find Buyer', 
  'mainpage_new_normal_desc'=>'In this time of e-commerce and digitalization has taken the first place, with ‘’new normal’’ exporters can sell their products and services to new customers on these channels.', 
  'mainpage_exporter_solutions_title'=>'Exporter Solutions', 
  'mainpage_exporter_solutions_desc_one'=>'Our uninterrupted export solutions will bring you together with your customers on digital channels. Grow your export with international market research, media planning, online export dynamics research, and much more solutions.', 
  'mainpage_exporter_solutions_desc_two'=>'planlama, e-ihracat dinamiklerini inceleme ve daha bir çok çözüm ile', 
  'mainpage_exporter_solutions_desc_three'=>'ihracatınızı büyütün.', 
  'quick_application_dots'=>'Quick Apply...', 
  'quick_application'=>'Quick Apply', 

  'big_exporters'=>'EXPORTERS', 
  'mainpage_exporters_title'=>'Search for new market places while exporting your business with the world’s first AI-based customer match tool', 
  'mainpage_exporters_desc'=>'Choose your market, conduct a market assessment online, analyze the data. Also, you can take a special data service.', 
  'mainpage_compaies'=>'Gelirlerini artıran 103 mutlu firma.', 
  'view_all'=>'View All', 

  'big_thinking_export'=>'THOSE WHO INTEND TO EXPORT', 
  'big_thinking_export_title'=>'If you haven’t started to export your business yet, start with smart and guiding exportation technology tools. Grow your exportation in multiple ways with the world’s first AI-based customer matching platform and many other tools.', 
  'big_thinking_export_desc'=>'Analyze the international market by looking at which counties are buying your products the most and an online market assessment. Don’t make hard work of starting export. Take your business a step forward with effective solutions.', 

  'big_e_export'=>'E-EXPORT', 
  'big_e_export_title'=>'Sell your products easily to the whole world with online export and grow your export on digital platforms. Start to sell abroad fast with web and mobile applications that are suitable for new markets.', 
  'big_e_export_desc'=>'Conduct a market assessment online and get special data service to reach out to the customers who are interested in your products.',

  'mainpage_es_one_title'=>'Export Technologies Consultant',
  'mainpage_es_one_desc'=>'It will help you with the technologies you will use while conducting market research, new customer matching and e-export planning. Make video conferences with your Export Technologies Consultant in every process so that your exports are uninterrupted.',
  'mainpage_es_two_title'=>'Reach to Customer Data',
  'mainpage_es_two_desc'=>'We identify companies that buy similar products from different countries and different exporters, down to location-based coordinate data, and extract all detailed information. In addition to these, if you want details up to contact information, we present this data to you in a monthly consultancy report.',
  'mainpage_es_three_title'=>'Market Research and Analysis with Artificial Intelligence',
  'mainpage_es_three_desc'=>'We offer all export data of the world for your use. By writing your HS Code, you can knit the countries and cities that buy your products the most by years. Which countries buy your products from which countries, you can see all the details. You can examine the markets related to your product and interpret import volumes.',
  'mainpage_es_four_title'=>'Data Mining Saves Your Future',
  //'mainpage_es_four_desc'=>'Storing all your potential customer data digitally makes it easier for us to access these customers. Segments are created according to your products, according to the content on your website and the authorized data of your visitors are stored. You can separate segments in all time frames such as 6 hours, daily, monthly, 365 days.',
  'mainpage_es_four_desc'=>'Storing all your potential customer data digitally makes it easier for us to access these customers. Segments get created according to your products and to the content on your website and the authorized data of your visitors get stored. You can separate segments in all time frames such as 6 hours, daily, monthly, 365 days.',
  'mainpage_es_five_title'=>'Protecting Your Brand Protects Your Business',
  'mainpage_es_five_desc'=>'The most important step when exporting to the markets you choose is to ensure that you have your trademark registration. We ensure that the suitability of your trademark registrations in these markets is investigated and then registered.',
  'mainpage_es_six_title'=>'Result: Increase in Exports by Customer Matching',
  'mainpage_es_six_desc'=>'We match you with buyers who buy your products from different companies in different markets through digital channels. You use the value of data, data mining to the fullest and you constantly appear in front of the right CUSTOMER. You improve your exports with customer data.',

  /*mainpage  finish */

    /*footer  start */

    'socialmedia_accounts' => 'Our social media accounts',
    'youcan_followus' => 'you can follow us on our social media accounts.',
    
    /*footer  finish */

    /*aplication form  start */
    'application_form' => 'Applycation Form',
    'application_form_content' => 'Application Form. You can start your application process by filling the form below.',
    'personal_information' => 'Personal Information',
    'company_information' => 'Company Information',
    'productname_or_code' => 'Product Name or Code',
    /*aplication form  finish */


    /*data yönetimi  start */
    'data_pageheader_title' => 'Increase e-exports using your own data.',
    'data_pageheader_content' => 'Every e-commerce site can sell abroad. The important thing is to get to know visitors to increase your sales abroad and to present the right product when they need it. Analyze your website visitors in detail according to their behavior. To increase your e-exports, get to know your visitors, create detailed segments based on their behavior on your website, and remind your products online by retargeting them.',
    'data_whatisthis' => 'WHAT IS IT AND HOW IT WORKS',
    'data_whatisthis_title' => 'Special segments for the countries you want to export.',
    'data_whatisthis_content_one' => 'Convert visitors to your website into customers with promotions and advertisements in the countries where you want to deliver your products. Separate the data of your geographic segments and authorized visitors on a country basis.',
    'data_pageheader_content_two' => 'For example ; Group your website visitors from Germany and UK visitors into separately targetable data segments. Create retargeting campaigns that can turn potential customers into new customers, specific to each country.',
    'main_features' => 'MAIN FEATURES',
    'data_mainfeatures_title' => 'Sort the visitors to your website based on their behavior.',
    'data_mainfeatures_content' => 'Every visitor to your website is a potential customer for you. Show the products they are interested in and can be interested in to convert the visitors you bring to your site into new customers with their long promotional efforts.',

    'data_rules_content_one' => 'With country-based segments, determine which country the visitors from your target market come from and make special ads in the language of the visitors.',
    'data_rules_content_two' => "Get to know your visitors' behaviors such as reviewing a product, adding a product to the basket, visiting a product more than once, and analyzing their needs.",
    'data_rules_content_three' => "Don't lose the first time visitors to your website, convert them into new customers. Increase purchasing rates by segmenting your customers according to their behavior and needs.",
    'data_rules_content_four' => 'Show the ads of the products your customers may need in the future by examining the products they have bought before.',
    'data_rules_content_five' => 'While increasing brand awareness in the markets you target, increase your sales with data management and data-targeted advertisements.',
    'data_rules_content_six' => 'While creating data segments, create your target audience by evaluating many rules such as the device used by the visitors, the products they visited or put into the basket, from which source they came to your site, and which page they visited.',
    'data_rules_content_seven' => 'Target people who interact with your products online with localized ads in their countries. Communicate with your potential customers in a way they can understand to encourage them to buy.',
    'data_rules_content_eight' => 'Upload your products with XML feeds and use the automatic segments to be created for each category and product in your campaigns as you wish.',
    'data_rules_content_nine' => 'Easily transfer your THEAD DSP integration segments to the DSP platform and quickly create retargeting campaigns.',

    'additional_features'=>'additional features',
    'date_features_title_one'=>'Our Technology Consultants Are Always With You',
    'date_features_content_one'=>'Our technology consultants are always with you to get to know and analyze your visitors. In order to increase e-export, we create targeted audiences from your own visitors with segments specific to your target markets and enable you to acquire new customers.',
    'date_features_title_two'=>'E-export in New Normal',
    'date_features_content_two'=>'Visitors use online shopping more intensively in the new normal. This will increase the number of visitors to your website from abroad. By analyzing incoming visitors, we identify your potential customers with advanced remarketing plans and enable you to reach these people in the countries you want to export.',
    'date_features_title_three'=>'The Power of Data for Cross Selling',
    'date_features_content_three'=>'We determine the products that visitors are interested in with not only geographical segments, but also detailed product segments, and we show other products to them that they might be interested in.  We prepare plans to improve your online cross-selling policy.',
    'date_features_title_four'=>'Do Not Think About Logistics and Cargo Processes',
    'date_features_content_four'=>'Thanks to the agreements we have made with major cargo companies such as DHL, which have access to everywhere in the world, we enable you to deliver your products to all over the world. We search companies suitable for your new target market for cargo operations and facilitate the logistics process.',
    
    /*data yönetimi  finish */

    /*marketing management  start */
    'emarketing_pageheader_title' => 'E-marketplace management',
    'emarketing_pageheader_content' => "Bring your products to global marketplaces
    While adding your products to local marketplaces, also add them to global marketplaces to improve your e-export. Let's do all your marketplace management from start to finish. Let's increase both your domestic and global sales together.",

    'emarketing_whatisthis_title' => 'Deliver your products all over the world.',
    'emarketing_whatisthis_content_one' => "We develop your e-exports by taking part in global marketplaces and promoting your products in target markets. We stand by you in store activation and product management processes in the world's largest marketplaces such as Amazon, ebay, and aliexpress.",
    'emarketing_whatisthis_content_two' => 'We follow the e-commerce and e-export trends for you, we ensure that you acquire new customers in global markets and reach your sales targets by acting in accordance with the new e-export dynamics in the marketplaces.',
    'emarketing_mainfeatures_title' => 'The most effective way to increase product sales in digital',
    'emarketing_mainfeatures_content' => 'The most effective way to increase your product sales both in the domestic market and in the global and local markets of the countries which you target is to take part in the marketplaces that are already familiar to the target audience and to promote your products',
    'emarketing_mainfeatures_content_two'=>"We perform all processes such as warehouse management by allowing you to take part in Turkey's leading marketplaces like Hepsiburada, GittiGidiyor, Trendyol, n11. At the same time, we undertake your store applications and product and shipment management processes for leading global marketplaces such as Amazon, Ebay, Ali express to improve your e-export.",

    'emarketing_rules_content_one'=>'We perform integration to all local and global marketplaces, store opening and product management, and sales-oriented campaign management.',
    'emarketing_rules_content_two'=>'We determine your target audience and increase product / audience interaction with appropriate methods and enable you to acquire new customers.',
    'emarketing_rules_content_three'=>'We enable you to reach potential customers By providing accurate analyzes for your products in the global marketplace and analyzing the needs and purchasing trends of the target audience',
    'emarketing_rules_content_four'=>'We strengthen the relationship between the seller and the customer with the right campaign management for your products and effective marketing techniques for your target audience.',
    'emarketing_rules_content_five'=>'We offer solutions for your overseas warehouse needs and facilitate your processes with our expert team in your overseas company setup processes.',
    'emarketing_rules_content_six'=>'With detailed reporting, we transparently present the way we have made in e-commerce in local marketplaces and the way we have made  in e-export in global marketplaces.',


    'emarketing_features_title_one'=>'Our Technology Consultants Are Always With You',
    //'emarketing_features_content_one'=>"You can constantly contact our expert team at all Isteps from the very beginning to the end of the process in marketplace management, , and ask any questions you may have. Let's decide together on how to follow your e-export journey with detailed planning and how to increase your sales.",
    'emarketing_features_content_one'=>"You can constantly contact our expert team at all Isteps from the very beginning to the end of the process in marketplace management and ask any questions you may have. Let's decide together on how to follow your e-export journey with detailed planning and how to increase your sales.",
    'emarketing_features_title_two'=>'E-export in New Normal',
    'emarketing_features_content_two'=>'Digital methods are gaining popularity in export transactions in the new normal. Keep up with the process by moving your exports to digital. To grow your e-exports, take your products to marketplaces that potential customers are familiar with and discover digital ways of opening up to the world.',
    'emarketing_features_title_three'=>'Get New Customers',
    'emarketing_features_content_three'=>'Get new customers from around the world without restricting your sales to the local market. Examine the markets of the countries you target with detailed market analysis, detail customer habits and purchasing trends. Acquire new customers by reaching the right target audience with effective marketing techniques.',
    'emarketing_features_title_four'=>'Make Conditions Suitable for e-export',
    'emarketing_features_content_four'=>"Let's explore all the details you need to know about branding processes, warehouse and logistics processes, steps and costs for establishing a company, taxes and laws of countries in order for you to open stores in global marketplaces and to do e-export together. Let's make your applications and run the product management without interruption.",

    /*marketing management  finish */

/*facebook-export  start */
'facebook_pageheader_title' => '‘’Take your company beyond the borders with Social Media” Get new customers from all over the world with Social Media.',
'facebook_pageheader_content' => 'Wherever your potential customers are, they are always spending time on Facebook and Instagram. We determine the most suitable audience for your products with target audience statistics and the "Cross-border statistics finder" to improve your exports with Social Media.',

'facebook_whatisthis_title' => 'Product - target audience relationship in social media.',
'facebook_whatisthis_content_one' => 'We create audiences of potential customers associated with your products, whether in the whole world or in the target countries you specify. We provide access to your products and specific target audiences suitable for your campaign goals such as conversion, traffic, video viewing, or similar target audiences that you can use in different countries.',
'facebook_whatisthis_content_two' => 'We ensure that your ads published in multiple languages ​​on Facebook and Instagram are shown to the people most relevant to your products.',
'facebook_mainfeatures_title' => 'Take your e-exports across the border on social media',
'facebook_mainfeatures_content' => 'You can grow your exports by targeting potential customers associated with your product among social media users. Many potential customers spend a lot of time on Facebook and Instagram. We create social media ads suitable for the target audience with different campaigns for all languages ​​you serve on your website.',

'facebook_rules_content_one' => 'With the statistics finder, we create the target audience suitable for your sector in your current market and create similar audiences in your target market suitable for this audience.',
'facebook_rules_content_two' => 'If you want, you can create special audiences for the country you intend to open to the market and reach people who meet the criteria you set. You can also target website visitors or mobile app users.',
'facebook_rules_content_three' => 'You can plan your ads according to the location of potential customers on Facebook and Instagram, and according to the country or city where they are located.',
'facebook_rules_content_four' => 'We match your product with potential customers on social media and help you increase your cross-border sales.',
'facebook_rules_content_five' => 'We create your ads in local languages ​​suitable for the target audience for customers in each country you target.',
'facebook_rules_content_six' => 'In the process of growing and developing your e-export on social media, we create your media plan and marketing strategy with our expert team, we stand by you in campaign management and all reporting processes.',

'fb_features_title_one' => 'Our Technology Consultants Are Always With You',
'fb_features_content_one' => 'Our technology consultants are always with you to get to know and analyze your visitors. In order to increase e-export, we create targeted audiences from your own visitors with segments specific to your target markets and enable you to acquire new customers.',
'fb_features_title_two' => 'CRM Integration',
'fb_features_content_two' => 'Visitors use online shopping more intensively in the new normal. This will increase the number of visitors to your website from abroad. By analyzing incoming visitors, we identify your potential customers with advanced remarketing plans and enable you to reach these people in the countries you want to export.',
'fb_features_title_three' => 'Social Media Retargeting',
'fb_features_content_three' => 'We determine the products that visitors are interested in with not only geographical segments, but also detailed product segments, and we provide the people who buy these products to show your other products that they can buy. We prepare plans to improve your online cross-selling policy.',
'fb_features_title_four' => 'Benefit from Government Support',
'fb_features_content_four' => 'Thanks to the agreements we have made with major cargo companies such as DHL, which have access to everywhere in the world, we enable you to deliver your products to all over the world. We search companies suitable for your new target market for cargo operations and facilitate the logistics process.',

/*facebook-export  finish */

      /*google_marketfinder  start */
      'gm_pageheader_title' => 'Sell your products to the whole world with Google Marketfinder.',
      'gm_pageheader_content' => 'Find new marketplaces and introduce your products to new customers from around the world with Google Marketfinder. Examine the potential customers who search for your products around the world and create your export plan.',

      'gm_whatisthis_title' => 'Market suggestions for your websites.',
      'gm_whatisthis_content_one' => 'We offer you market suggestions that shapes by online shopping behaviors of that country and that are suitable for your product categories. We examine your product categories together and make a detailed market research with google marketfinder exportation tool.',
      'gm_whatisthis_content_two' => 'We examine the number of people searching for the products you sell in the target countries, the economic conditions of the countries, the laws required to be suitable for export, and many other data and determine the most suitable markets for you.',
      'gm_mainfeatures_title' => 'Online Searcing and purchasing data-focused exportation planing ',
      'gm_mainfeatures_content' => 'We answer many questions together like what are the categories of the products you sell, how much are the google search of these products on the world, what are the buying tendencies of the people from your targeted country, how are their incomes, are there any difficulties in logistics or law',
      'gm_mainfeatures_content_two' => 'After we determine the convenient markets we make plans to better your websites, grow your online exportation and increase the sales with advertisements.',



      'gm_rules_content_one' => 'We scan your web site and determine your product categories. You can add more categories if you want to.',
      'gm_rules_content_two' => 'We determine the most appropriate markets according to the online rigging data for your categories. We examine Google ads offers for the campaigns in these markets.',
      'gm_rules_content_three' => 'We examine the profits of the countries and their economies so we can offer information about the purchasing tendency and economic status of the people who live there.',
      'gm_rules_content_four' => 'We help you with many things that businesses usually struggle with like website localization, international payment, and logistics.',
      'gm_rules_content_five' => 'We create detailed social media plans for Google ads campaigns by country, we ensure that you introduce your products to the right people you should be targeted. We help you to make a complete marketing strategy.',
      'gm_rules_content_six' => 'We answer many questions in your mind like which devices your potential customers are using, which language is the best for your advertisement campaigns, which advertising models you can use to reach your customers. We follow the process of advertisement making and management.',

      
      'gm_features_title_one' => 'Website Localization',
      'gm_features_content_one' => 'Translating the content on your website may not be enough for you to be understood by the target audience. False translations can make it harder for you to reach out the audience. For your ads and website to be understandable, you need to localize your content in accordance with the languages ​​used by people living in the target countries. We assist in all content editing processes.',
      'gm_features_title_two' => 'Detailed Media Plan and Ad Management',
      'gm_features_content_two' => 'After the analysis with Google marketfinder, we prepare a detailed media plan for Google ads campaigns. We prepare all the details such as which advertising models you should use, which words are your potential customers searching for, and what language you should use to reach the target audience. We take all the burden in the creation, management and reporting processes of your Google ads campaign and we stand by you in all of your e-export processes.',
      'gm_features_title_three' => 'International Payments, Taxes, and Legal Processes',
      'gm_features_content_three' => 'Your e-export process does not only consist of product promotion. We detail and report whether there are any difficulties awaiting you in the target countries regarding payment, tax, logistics, and legal processes. We determine the costs required for you to be suitable for export in the country you are targeting.',
      'gm_features_title_four' => 'Benefit from Government Support',
      'gm_features_content_four' => 'Benefit from the state supports up to 250000 USD given to companies that aim to export or increase their exports by advertising products abroad. Google Ads is also one of the platforms that you can benefit from government support.',
      

      /*google_marketfinder  finish */

      /*media_planning  start */
      'media_pageheader_title' => 'Improve your exports with a strong media plan.',
      //'media_pageheader_content' => 'Let us prepare a powerful export-oriented and versatile social media plan to create brand awareness, and to promote your brand and products in target markets, identify and reach potential customers globally. We help you reach your goal with detailed plans for all advertising and promotional activities aimed at increasing product sales in global markets.',
      'media_pageheader_content' => 'We prepare a powerful export-oriented and versatile social media plan to create brand awareness and to promote your brand and products in target markets, and also to identify and reach potential customers globally. We help you reach your goal with detailed plans for all advertising and promotional activities aimed at increasing product sales in global markets.',
      'media_whatisthis_title'=>'Target the potential CUSTOMER',
      'media_whatisthis_content'=>'Target potential customers who might be interested in your products. Let us determine the target audience you want on the basis of sectoral or vertical data abroad and let us publish your ads on the web and mobile sites you want.',
      'media_whatisthis_content_two'=>'The road to big goals goes through big data. With the data-oriented detailed media plan, we create a target audience specific to you and on the target country basis and ensure that the relevant people see your products.',
      'media_mainfeatures_title'=>'Media planning that keeps pace with the digital transformation',
      'media_mainfeatures_content'=>'The need for digital transformation and media planning for this transformation has become stronger In the new normal. We help you to interact with your potential customers on digital channels, to create your detailed digital media plan in line with your marketing goals, and to gain new customers with new visitors.',
      'media_mainfeatures_content_two'=>'We prepare a mass-oriented and versatile media plan specific to your company and your products, and ensure that your ads are delivered to the target audience both on websites abroad and on social media.',
      'media_rules_content_one'=>'We create your programmatic advertising plan to create brand awareness and attract new visitors in the countries you are targeting to export. We ensure that the image or video ads of your brand are seen by the appropriate target audience on the web or mobile sites you want.',
      'media_rules_content_two'=>"With THEADX EXPORT, the world's first and only artificial intelligence-based export planning tool, we identify companies that buy your products on a country basis and ensure that your ads are continuously displayed in the locations of these companies.",
      'media_rules_content_three'=>'On the basis of the country you want to export, we enable you to reach people with a higher probability of purchasing instead of random ad display with both sectoral and vertical audience segmentation specific to your products.',
      'media_rules_content_four'=>'With visitor analysis, we create specific target audiences from your website visitors, and we constantly remind them of the products and your brand online with dynamic ads specific to these audiences.',
      'media_rules_content_five'=>'For every country you want to export, we enable you to interact with localized ads in a language your target audience understands. We plan your ads to be shown to the right people at any time in the country or city you want.',
      'media_rules_content_six'=>'We research, elaborate, and plan the programmatic purchasing processes, advertising models you will use, how much budget you should spend on which channel, goals and conversions for each channel, in short, how you should interact with customers in digital channels.',
      'media_features_title_one'=>'Continuity of Marketing',
      'media_features_content_one'=>'Be in constant contact with our technology consultants during the market research process, while determining suitable countries for you or examining the import volume of your target country. Conduct video conferences with our expert team in market analysis and media plan operations.',
      'media_features_title_two'=>'Customer Focused Advertising Plan',
      'media_features_content_two'=>'Store the authorized data of visitors who visit your website and examine your products with online promotions. Offer personalized benefits to your visitors and constantly remind your products by including visitor data-driven dynamic ads in your media plan.',
      'media_features_title_three'=>'Detailed Reporting and Conversion Tracking',
      'media_features_content_three'=>'We present all the details of your expenses in your digital media marketing and promotional activities with advanced reports and track the conversions. Increase conversion rate with export-oriented and data-aware ads.',
      'media_features_title_four'=>'Benefit from Government Support',
      'media_features_content_four'=>'Thanks to the agreements we have made with major cargo companies such as DHL, which have access to everywhere in the world, we enable you to deliver your products all over the world. For cargo operations, we search companies suitable for your new target market and facilitate the logistics process.',
      

      /*media_planning  finish */

      /*online_sale_website  finish */
      'osw_pageheader_title'=>'Strong e-commerce website, Successful e-export',
      'osw_pageheader_content'=>'The success of our promotional advertising activities to improve your e-exports are dependent on how strong your e-commerce site is. Sell ​​your products all over the world with an e-commerce site that can be integrated quickly with global e-markets, equipped with global and advanced modules, localized in a way that people in your target market can understand.',
      'osw_whatisthis_title'=>'Set up your global e-commerce site now to open up abroad',
      'osw_whatisthis_content'=>'In order to start global sales and increase your sales, we are with you every step of the way while setting up your professional e-commerce site that can be understood by people from all over the world.',
      'osw_whatisthis_content_two'=>'We enable you to quickly activate your site without having to think about site setup, language support suitable for your target markets, localization, logistics support and many issues.',
      
      'osw_mainfeatures_title' => 'Enter the global markets that you target with fast and firm steps.',
      'osw_mainfeatures_content' => 'Make a strong start to e-export with your easy to use e-commerce site with the most suitable design and infrastructure for you. We offer together all the features that will make it easier for you to sell all over the world and enable you to manage all processes from a single panel.',
      
      'osw_rules_content_one' => 'We enable you to provide an enhanced user experience on every device and to your visitors with professional modules and responsive design that you can use in national or international markets,',
      'osw_rules_content_two' => 'Easily manage language settings as suitable to your target markets for your products with multi-language support and localization service. With localization, we help you make language entries in a way that your target audience can understand without translating your content exactly.',
      'osw_rules_content_three' => 'We enable you to professionally manage currency units, customs and tax deals in your global sales, as country-based.',
      'osw_rules_content_four' => 'We offer an advanced admin panel where you can manage country-based products and prices. You can price your products differently for each country and manage the currency.',
      'osw_rules_content_five' => 'With our integrations with international shipping companies such as UPS and DHL, we facilitate the process of sending products to all over the world.',
      'osw_rules_content_six' => 'With the location-based visitor recognition feature, we ensure that your products are displayed in the language and currency appropriate to the country of your e-commerce site. We help you to easily understand your products at the opening so that you can convert the visitor into a customer.',

      'osw_features_title_one' => 'Easy Installation, Fast e-export',
      'osw_features_content_one' => 'Our technology consultants are with you in all steps while activating your e-commerce site in the national and global market. We facilitate many processes such as site setup, advanced and SEO compatible design, XML, API and CSV files, product management, language, currency and price management.',
      'osw_features_title_two' => 'Global Marketplace Integrations',
      'osw_features_content_two' => 'Add your products to well-known marketplaces such as Ebay, Amazon, and Aliexpress which the target audience is used to, in order to be able to sell your products more easily and to increase brand awareness. With the integrations you add to your e-commerce site, you can easily add them to global marketplaces.',
      'osw_features_title_three' => 'Virtual Pos and Payment System Management',
      'osw_features_content_three' => 'We offer a virtual pos management facility where you can receive payments from abroad so that you can sell and receive payments in your target markets. In addition, we also offer methods that you can easily receive payments through integrations with dozens of payment systems such as iyzico, payu, payguru, and paytrek.',
      'osw_features_title_four' => 'B2B and B2C Sales Convenience',
      'osw_features_content_four' => 'We facilitate your B2B and B2C sales with product, pricing, tax, customs and currency management on target country basis. We evaluate the e-export process as a whole and we stand by you in every aspect such as order, payment and logistics so that you can sell your products in global markets.',
      

      
      /*online_sale_website  finish */

      /* theadx_export  start */
      //'adx_pageheader_title' => ' Worldwide Exports are Within Arms Reach Theadx Export',
      'adx_pageheader_title' => ' Worldwide Exports are Within Arms Reach Artificial Intelligence Supported Export',
      'adx_pageheader_content' => "With the world's first artificial intelligence-based export planning tool, enter your HS Code and examine the export data of the whole world with all the details on a yearly basis. Match your products with import companies",
      'adx_whatisthis_title' => 'All the data you need for export.',
      'adx_whatisthis_content' => 'It is possible to find the answers of many questions like which countries buy your product, from which countries make more purchases, in which years the export is more and which companies buy from the importing countries,',
      'adx_whatisthis_content_two' => 'We offer all the data you will need while exporting or just starting to export.',
      'adx_mainfeatures_title' => 'Improve your exportation step by step. Detailed market research, data mining, and media planning.',
      'adx_mainfeatures_content' => 'By entering your HS Code, examine the trade volume of your product in countries, product-based trade status, export growth rates depending on years. You can even examine the export status of your product in certain countries and make a detailed market analysis. You can match importing companies with your product in the countries you want and examine the companies that buy that kind of products.',
      'adx_rules_content_one' => 'We present the export data for the last 30 years, either worldwide or on a country basis. Examine the markets you want to open in detail before exporting.',
      'adx_rules_content_two' => 'We identified companies that purchased products based on HS codes used in export transactions worldwide. Check out potential CUSTOMERS who are already purchasing your product from other countries or other exporters.
      ',
      'adx_rules_content_three' => 'We have eliminated questions such as how much you should display in which country or location of the company, how much budget you need to allocate. We present a ready-made detailed media plan for online access to companies that purchase products and export volumes.',
      'adx_rules_content_four' => 'We offer you the locations of the importing companies with Artificial Intelligence algorithms. We constantly show your advertisements at the locations of the companies that will buy your product.',
      'adx_rules_content_five' => 'We enable you to reach 5.8 million companies worldwide and show your products to the right target audience in the right way.',
      'adx_rules_content_six' => 'We offer private data services for your products. We match your products with potential customers and determine the right target audience for you.',

      'adx_features_title_one' => 'Our Technology Consultants Are Always With You',
      'adx_features_content_one' => 'Be in constant contact with our technology consultants during the market research process, while determining suitable countries for you or examining the import volume of your target country.  Set up video conferences with our expert team in market analysis and media plan operations.',
      'adx_features_title_two' => 'Keep Up with the "NEW NORMAL"',
      'adx_features_content_two' => 'In the new normal market analysis will no longer be easy at its place. By doing your market research online, reduce your costs and perform comprehensive analysis. View the export data of the whole world, not just certain countries, on a single screen.',
      'adx_features_title_three' => 'Store Your Data Digitally',
      'adx_features_content_three' => 'Store the users who visit your website and view your products online with online promotions. Remind your products constantly by retargeting people who show an interest in your promotions',
      'adx_features_title_four' => 'Benefit from Government Support',
      'adx_features_content_four' => 'Benefit from the state supports up to 250000 USD given to companies that aim to export or increase their exports by advertising products abroad. THEADX is among the platforms where you can benefit from government support.',
      
      

      /*theadx_export  finish */      

      /*together_social  start */
      //'ts_pageheader_title' => 'Take your brand one step further with trendsetters in target markets',
      'ts_pageheader_title' => 'Take your brand one step further in target markets with influencers and content distribution networks',
      //'ts_pageheader_content' => 'Do not settle for events, fairs, or ad campaigns for brand and product promotion in the markets of the countries you target. Promote your products in countries you want to export, with effective marketing techniques you will do with well-known people on blogs, websites, social media without creating a perception of advertising with content distribution.',
      'ts_pageheader_content' => 'Do not settle for events, advertisements, or fairs for brand and product promotion in the markets of the countries you target. Promote your products with influencer marketing and content distribution in countries you want to export, with effective marketing techniques you will do with well-known people on blogs, websites, and social media without creating an advertisement perception.',
      'ts_whatisthis_title' => 'Stand out in the target market with trendsetters.',
      'ts_whatisthis_content' => 'Deliver your products to different audiences all over the world on social media in the countries you target. With our global influencer network, we take your social media ads out of a simple post, and we enable you to experience the influencer marketing process in its best quality with creative and remarkable sharing.',
      'ts_whatisthis_content_two' => 'Access your target audience more easily through the people they follow, love, and trust with a catchy content.',


      'ts_mainfeatures_title' => 'Promote your products naturally to the target audience in the countries you want to export',
      'ts_mainfeatures_content' => 'With our global content distribution network, we implement innovative marketing techniques that will allow your target audience to recognize your products in natural ways beyond advertising, and we ensure that you reach the right audience with effective methods with detailed plans tailored for your target countries.',


      'ts_rules_content_one' => 'We enable different people to see content related to your products on social media or in our content distribution network with unique target audiences globally.',
      'ts_rules_content_two' => 'We ensure that the content of your pages on social media platforms such as Facebook, Instagram, and Linkedin reach more people and we increase the engagement.',
      'ts_rules_content_three' => 'We deliver your content or product pages that you want to be seen by the target audience on your website to more people with our global content distribution network and help you increase brand awareness naturally.',
      'ts_rules_content_four' => "We ensure that your products or your brand's video promotions are watched by more people in the country you want.",
      'ts_rules_content_five' => 'We produce innovative and creative content with well-known influencers on social media in the country you are targeting and help you promote your products all over the world.',
      'ts_rules_content_six' => 'For each country you target, we prepare detailed plans with effective marketing techniques in our content distribution network, evaluate and report every step to the finest detail.',

      'ts_features_title_one' => 'Unique Target Audience Analysis in Target Markets',
      'ts_features_content_one' => 'We create unique target audiences for each method in the process of product promotion, video watching, distribution of content related to your products in the countries you want to export, and ensure that your products are viewed by more individual people.',
      'ts_features_title_two' => 'Use the Power of Social Media to Export',
      'ts_features_content_two' => 'Go beyond the usual posts on social media. In product and brand promotion processes, which are indispensable for e-exports, we include your social media pages, popular blogs, news sites, and social media in the countries you want, and we develop your e-export.',
      'ts_features_title_three' => 'More Engagement More Purchase',
      'ts_features_content_three' => 'We enable your products to interact with the target audience more than the perception of advertising. We go beyond conventional digital marketing methods, integrate your product and content, and deliver it to potential customers.',
      'ts_features_title_four' => 'Innovative Marketing Methods in e-export',
      'ts_features_content_four' => 'We prepare detailed marketing plans for you to reach new markets with innovative methods. We conduct extensive research on social media and content distribution in each country you target and determine the appropriate publisher and target audience for you.',
      
      /*together_social  finish */

      /*web_mobil_gelistirme  start */
      'wmd_pageheader_title' => 'Export oriented web and mobile application',
      'wmd_pageheader_content' => "Before you start exporting, we help you organize your website and mobile app in a way that visitors from your target markets will understand. We develop an understandable, SEO-compatible web and mobile application for you in order not to waste your budget for promotional activities for digital marketing and to convert visitors into customers.",
      'wmd_whatisthis_title' => "Let's lay the foundation of successful export together.",
      'wmd_whatisthis_content' => 'Visitors to your website from the countries you want to export to recognize you as you describe them on your website. Greet visitors with a fast, clear and SEO-compliant website. Streamline user experience by country.',
      'wmd_whatisthis_content_two' => 'We are with you in the process of localizing your content, such as how your website content should be in the countries you are targeting to export, how product names are used in the target market. We develop your web and mobile application in the most accurate way to describe your brand and products.',
      'wmd_mainfeatures_title' => 'Successful user experience with advanced web and mobile application',
      'wmd_mainfeatures_content' => "Let's develop your applications that prioritize user experience, reveal your products in their clearest form and reflect your brand in the most professional way. Let's move your brand one step forward in the countries you are targeting to export with local SEO studies for the target market.",
      'wmd_mainfeatures_content_two' => "Let's develop your professional web and mobile application to convert the visitors who you get with long efforts into customers by allocating certain budgets",
      
      'wmd_rules_content_one' => 'Describe your products on all devices and resolutions with web and mobile compatible designs. With responsive designs, we ensure that visitors can see your content and products properly, regardless of which device they connect to.',
      'wmd_rules_content_two' => 'Welcome visitors in their own language. While welcoming people who connect to your website from Germany with German content, show Russian content to visitors from Russia.',
      'wmd_rules_content_three' => 'In the countries you want to export, we analyze many details that will highlight you such as keyword planning and search volumes, and we ensure that the application content is local seo compatible.',
      'wmd_rules_content_four' => 'We keep the user experience at the forefront, with high quality and fast infrastructure, we ensure that the visitor is informed about your products without making them bored and lost focus.',
      'wmd_rules_content_five' => 'Get your native or web-based mobile application that will run on IOS and Android platforms. We develop your mobile application by keeping the mobile experience of your users at the forefront.',
      
      'wmd_features_title_one' => 'UI / UX Design',
      'wmd_features_content_one' => 'The web and mobile interface designs that prioritize the user experience, help you to provide personalized experiences to the visitors on your applications
      with the developments that prioritize the user experience. We enable visitors to spend more time on your website or mobile application.',
      'wmd_features_title_two' => 'Export & Digital Transformation',
      'wmd_features_content_two' => 'We ensure that you perform the application development step of the digital transformation process smoothly in order to reach your target audience in the countries you are targeting to export in the most accurate way. We help you to be understood by the target audience and to interact with your brand and products more easily.',
      'wmd_features_title_three' => 'Mobile Application & Brand Belonging',
      'wmd_features_content_three' => 'With mobile applications, we ensure that your users can reach you from mobile devices at any time. The shifting of the usage habits of internet users to mobile devices strengthens the need for mobile applications. We ensure that you are always accessible and connect your users with your products.',
      'wmd_features_title_four' => 'Professional Team, Quality Service',
      'wmd_features_content_four' => 'We carry out the design, development and all testing processes of your web and mobile application from the planning stage with our professional team who are experts in their work. We keep in touch with you at every step and ensure that your applications are created in accordance with your request and in the best quality.',
      

      /*web_mobil_gelistirme  finish */

        /*cotactus  start */
        'contact_contactus' => 'Fill out the form to contact us.',
        'contact_name_surname' => 'Name and Surname',
        'contact_email' => 'E-Mail Address',
        'contact_howcanhelp' => 'How can we help you?',
        'contact_phone_number' => 'Your Mobile Number ',
        'send_message' => 'Send Message',
        'send' => 'Send',
        'send_form' => 'Submit Form',
        'cancel' => 'Cancel',
        'contact_title' => 'Title',
        'contact_company_title'=>'Company Trade Name',
        'export_authority'=>'export authority',

        /*cotactus  finish */
        'getoffer' => 'Get Offer',
        'all_rights_reserved' => 'All Rights Reserved',

        
        /*contactform  start */
        
        'write_name' => 'Write your name and surname',
        'invalid_email' => 'Invalid Email',
        'message_notfound' => 'Message not found.',
        'contact_error_message' => 'There was an error while transmitting information to us.',
        'please_email' => 'Please email.',
        'received_your_info' => 'Your information has been received.',
        'get_back_soon' => 'we will get back to you as soon as possible.',

        'contact_phone' => 'Phone',
        'contact_company' => 'Company',
        'contact_form' => 'Contact Form',
        'contact_form_desc' => 'You can start your application process by filling the form below.',
        'message' => 'Messages',
        'contact_info' => 'Contact Information',
        'contact_address' => 'Address',
        'email' => 'E-mail',
        /*contactform  finish */

        /*export consult  start */
       'team' => 'Team',
       'team_pageheader_title' => 'Meet the team members.',
       'team_synergy' => 'Your product or service succeeds with the synergy of this Team.',
       'team_member_name_one' => 'Emrah Pamuk',
       'team_member_title_one' => 'Co-Founder & CEO',
       'team_member_desc_one' => 'Experienced Co-Founder with a demonstrated history of working in the marketing and advertising industry for more than 13 years. Strong, committed, result-oriented, passionate and engaging business leader with strategic and executional expertise on digital & offline marketing management with high communication and people management skills...',
       'team_member_desc_two'=>'Keep in Digital',


       'team_member_name_two' => 'Handan KÜTÜK',
       'team_member_title_two' => 'Business Development Director',
       'team_membertwo_desc_one' => '',
       'team_membertwo_desc_two'=>'',

       'team_member_name_three' => 'Rinet HOŞOL',
       'team_member_title_three' => 'Head of Programmatic & Facebook',
       'team_memberthree_desc_one' => '',
       'team_memberthree_desc_two'=>'',
       
       'team_member_name_four' => 'Barış YILMAZ',
       'team_member_title_four' => 'Media Purchasing Relations Director',
       'team_memberfour_desc_one' => '',
       'team_memberfour_desc_two'=>'',

       'team_member_name_five' => 'Ozan ÇAKMAK',
       'team_member_title_five' => 'Social Media Manager',
       'team_memberfive_desc_one' => '',
       'team_memberfive_desc_two'=>'',

       'team_member_name_six' => 'Dilara DAĞISTANLI',
       'team_member_title_six' => 'Business Development Specialist',
       'team_membersix_desc_one' => '',
       'team_membersix_desc_two'=>'',

       'team_member_name_seven' => 'Ceren MUSLU',
       'team_member_title_seven' => 'Business Development Specialist',
       'team_memberseven_desc_one' => '',
       'team_memberseven_desc_two'=>'',

       'team_member_name_eight' => 'Erman PAMUK',
       'team_member_title_eight' => 'Art Director',
       'team_membereight_desc_one' => '',
       'team_membereight_desc_two'=>'',

       'team_member_name_nine' => 'Serra ÇETİNKAYA',
       'team_member_title_nine' => 'Social Advertising Specialist',
       'team_membernine_desc_one' => '',
       'team_membernine_desc_two'=>'',

       'team_member_name_ten' => 'Mısra VOLKAN',
       'team_member_title_ten' => 'Brand Manager',
       'team_memberten_desc_one' => '',
       'team_memberten_desc_two'=>'',

       'team_member_name_eleven' => 'Dilge ÇETİNÇAY',
       'team_member_title_eleven' => 'Content Manager',
       'team_membereleven_desc_one' => '',
       'team_membereleven_desc_two'=>'',

       'team_member_name_twelve' => 'Kami GENÇBARONYAN',
       'team_member_title_twelve' => '',
       'team_membertwelve_desc_one' => '',
       'team_membertwelve_desc_two'=>'',

       'team_member_name_thirteen' => 'Efe ALTINELLİ',
       'team_member_title_thirteen' => 'E-commerce Performance Manager',
       'team_memberthirteen_desc_one' => '',
       'team_memberthirteen_desc_two'=>'',

       'team_member_name_fourteen' => 'Merve ANAÇ',
       'team_member_title_fourteen' => 'Performance Manager',
       'team_memberfourteen_desc_one' => '',
       'team_memberfourteen_desc_two'=>'',

       'team_member_name_fifteen' => 'Uğur AKÇIL',
       'team_member_title_fifteen' => 'Senior Lead Developer',
       'team_memberfifteen_desc_one' => '',
       'team_memberfifteen_desc_two'=>'',

       'team_member_name_sixteen' => 'Erol TUNA',
       'team_member_title_sixteen' => 'Frontend Developer',
       'team_membersixteen_desc_one' => '',
       'team_membersixteen_desc_two'=>'',

       'team_member_name_seventeen' => 'Efecan GÜLER',
       'team_member_title_seventeen' => 'Business Development Manager',
       'team_memberseventeen_desc_one' => '',
       'team_memberseventeen_desc_two'=>'',

       'team_member_name_eighteen' => 'Bahadır DİNDAR',
       'team_member_title_eighteen' => 'Sr. UX/UI Designer',
       'team_membereighteen_desc_one' => '',
       'team_membereighteen_desc_two'=>'',
       /*export consult  finish  */

             /*collobrators consult  start */
      'our_collaborators' => 'Our Collaborators',
      'col_pageheader_title' => 'Our Collaborators',
      'col_technology_partner_title' => 'Our Customers Working With Us',

      /*collobrators consult  finish  */

      /*partners   start */
      'partners_pageheader_title' => 'Partners',
      'technology_partners' => 'Technology Partners',
      'bussines_partners' => 'Business Partners',
      'data_partners' => 'Data Partners',

      /*partners   finish  */
);

?>