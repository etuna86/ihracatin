<?php
$lang=array(

    'ihracatin' => 'ihracat.in',

    /*metadescription  start*/
    'metadesc_mainpage' => 'İşinizi geliştirmek ve yeni pazarlara açılmak için anasayfamızdan ihracatçı çözümlerimize göz atabilirsiniz.',
    'metadesc_google_market_finder' => 'Google Marketfinder ile tüm dünyadan yeni müşterilere ürünlerinizi tanıtın ve yeni pazarlara açılın.',
    'metadesc_theadx_export' => 'Yapay zeka tabanlı ihracat planlama aracı TheADX Export ile tüm dünyanın ihracat verilerini detaylı şekilde inceleyin.',
    'metadesc_together_social' => '2gether Social ile ürünlerinizi reklam algısı yaratmadan sosyal medyada tanınmış kişilerle etkili pazarlama teknikleriyle tanıtın.',
    'metadesc_media_planning' => 'Size özel ve güçlü bir medya planlama ile markanızı hedef pazarlarda tanıtarak globalde potansiyel müşterilerinizi belirleyin.',
    'metadesc_online_sales_site' => 'Hedeflediğiniz pazara göre yerelleştirilmiş ve donatılmış online satış sitesi ile ürünlerinizi dünyanın dört bir yanına satın.',
    'metadesc_dmp' => 'Data Yönetimi (DMP) ile ziyaretçileriniz tanıyın, web sitenizdeki davranışlarına bağlı detaylı segmentler oluşturun.',
    'metadesc_web_mobil_app' => 'Web mobil uygulama geliştirme ile ziyaretçilerinizi müşteriye dönüştürmek için SEO uyumlu web ve mobil uygulamanızı geliştirin. ',
    'metadesc_emarketing' => 'E-pazar yeri yönetimi ile ürünleri yerel pazar yerlerine eklerken e-ihracatınızı geliştirmek için global pazar yerlerine ekleyin.',
    
    'metadesc_socialmedia_export' => 'Sosyal Medya Export ile firmanızı sınırların ötesine taşıyın, dünyanın dört bir yanından yeni müşteriler edinin.',
    'metadesc_export_consultant' => 'Ürününüz ya da hizmetiniz, ihracat danışmanlarımızın takım sinerjisiyle başarıya ulaşıyor.',
    'metadesc_referances' => 'Referanslarımız sayfasından bizimle çalışan markalarımıza göz atabilirsiniz.',
    'metadesc_partners' => 'Partnerlerimiz sayfasından teknoloji partnerlerimize göz atabilirsiniz.',
    'metadesc_contact' => 'İletişim sayfasından iletişim bilgilerimizi öğrenebilir, dilerseniz iletişim formu doldurarak bize ulaşabilirsiniz.',
    
    /*metadescription finish*/

    /*header  start */
 
    'mainpage' => 'Ana Sayfa',
    'exporter_solution' => 'İhracatçı Çözümleri',
    'exporters' => 'İhracat Yapanlar',
    'thinking_export' => 'İhracat Yapmayı Düşünenler',
    'e_export' => 'E-İhracat',
    'products' => 'Ürünler',
    'google_marketfinder' => 'Google Market Finder',
    //'theadx_export' => 'TheADX Export',
    'theadx_export' => 'Yapay Zeka ile Export Management',
    //'together_social' => '2gether Social',
    'together_social' => '2gether Social - Influencer ve İçerik Dağıtım Networkü',
    'media_planning' => 'Medya Planlama',
    'online_sale_site' => 'Online Satış Sitesi',
    'data_management' => 'Data Yönetimi (DMP)',
    'web_mobil_application_develop' => 'Web Mobil Uygulama Geliştirme',
    'emarketing_management' => 'E-Pazar Yeri Yönetimi',
    'facebook_export' => 'Sosyal Medya Export',
    'we_doing' => 'Ne Yapıyoruz ?',
    'market_research' => 'Pazar Araştırması',
    'planning_with_ai' => 'Yapay Zeka ile Pazarlama',
    'customer_data' => 'Müşteri Verisi',
    'data_mining' => 'Veri Madenciliği',
    'create_brand' => 'Marka Oluşturma',
    'export_consultants' => 'İhracat Danışmanları',
    'collaborators' => 'Referanslarımız',
    'partners' => 'Partnerlerimiz',
    'government_supports' => 'Devlet Destekleri',
    'contact' => 'İletişim',
    /*header  finish */

    /*mainpage  start */
    'mainpage_pageheader_title' => 'Yeni Normal’de İhracat Nasıl Yapılır ? ',
    'mainpage_pageheader_desc' => 'Dünyanın ilk yapay zeka tabanlı ihracat analiz ve müşteri eşleştirme aracı ile işinizi geliştirin ve yeni pazarlara açılın, ihracata başlayın ya da ihracatınızı büyütün.',
    'mainpage_product_name'=>'Ürün adınız veya kodunu yazınız', 
    'mainpage_find_buyer'=>'Alıcı Bul', 
    'mainpage_new_normal_desc'=>'E-ticaretin ve dijitalleşmenin en önde yer aldığı bu son dönemde “YENİ NORMAL” ile artık ihracatçılar da ürünlerini ve hizmetlerini bu kanallarla elde edecekleri müşterilere satabilecekler.', 
    'mainpage_exporter_solutions_title'=>'İhracatçı Çözümleri', 
    'mainpage_exporter_solutions_desc_one'=>'Kesintisiz ihracat için çözümlerimiz sizi dijital kanallar üzerinden alıcılarınızla buluşturacak. Uluslararası pazar araştırması, medya planlama, e-ihracat dinamiklerini inceleme ve daha birçok çözüm ile ihracatınızı büyütün.', 
    'mainpage_exporter_solutions_desc_two'=>'planlama, e-ihracat dinamiklerini inceleme ve daha bir çok çözüm ile', 
    'mainpage_exporter_solutions_desc_three'=>'ihracatınızı büyütün.', 
    'quick_application_dots'=>'Hızlı Başvuru...', 
    'quick_application'=>'Hızlı Başvuru', 

    'big_exporters'=>'İHRACAT YAPANLAR', 
    'mainpage_exporters_title'=>'İhracat Yaparken Dünyanın #ilk Yapay Zeka Tabanlı Müşteri Eşleştirme Platformu ile Yurtdışında Yeni Pazarlar Arayın', 
    'mainpage_exporters_desc'=>'Piyasanızı seçin, piyasa değerlendirmesini online yapın, verileri analiz edin. Ayrıca özel veri hizmeti alın.', 
    'mainpage_compaies'=>'Gelirlerini artıran 103 mutlu firma.', 
    'view_all'=>'tümünü görüntüle.', 

    'big_thinking_export'=>'İHRACAT YAPMAYI DÜŞÜNENLER', 
    'big_thinking_export_title'=>'İhracata henüz başlamadıysanız, Akıllı ve Yol Gösterici İhracat Teknoloji Araçları ile başlayın. Dünyanın #ilk yapay zeka tabanlı Müşteri Eşleştirme Platformu ve daha birçok araç ile ihracatınızı çok yönlü geliştirin.', 
    'big_thinking_export_desc'=>'Ürününüzü hangi ülkelerin en çok aldığına bakarak ve online piyasa değerlendirmesi yaparak tüm dünya pazarlarını analiz edin. İhracata başlamayı artık gözünüzde büyütmeyin. Kullanışlı ve etkin çözümler ile işinizi bir adım öteye taşıyın.', 

    'big_e_export'=>'E-İHRACAT', 
    'big_e_export_title'=>'Ürünlerinizi tüm dünyaya e-ihracat ile kolayca satın ve ihracatınızı dijitalde geliştirin. Yurt dışında yeni pazarlara uygun web ve mobil uygulamalar ile hızlıca yurt dışına satışlarınızı başlatın.', 
    'big_e_export_desc'=>'Online piyasa araştırması yapın ve ürünleriniz ile ilgilenen potansiyel müşterilere erişmek için özel veri hizmeti alın.',

    'mainpage_es_one_title'=>'İhracat Teknolojileri Danışmanı',
    'mainpage_es_one_desc'=>'Pazar araştırması yaparken, yeni müşteri eşleştirmesi ve e-ihracat planlamalarını sağlarken kullanacağınız teknolojilerde size yardımcı olacak. İhracatınızın kesintisiz olması için İhracat Teknolojileri Danışmanınız ile her süreçte video konferans yapın.',
    'mainpage_es_two_title'=>'Müşteri Verilerine Ulaş',
    'mainpage_es_two_desc'=>'Ürettiğiniz ürünlerin benzerlerini farklı ülkelerden, farklı ihracatçılardan alan firmaları, lokasyon bazlı koordinat verilerine kadar tespit edip, tüm detay bilgilerini çıkarıyoruz. Bunların yanı sıra eğer iletişim bilgilerine kadar detay isterseniz, bu verileri size aylık danışmanlık raporu ile sunuyoruz.',
    'mainpage_es_three_title'=>'Yapay Zeka ile Pazar Araştırması ve Analizi',
    'mainpage_es_three_desc'=>'Dünyanın tüm ihracat verilerini kullanımınıza sunuyoruz. GTİP kodlarınızı yazarak, ürünlerinizi en çok alan ülkeleri ve şehirlerini yıllara göre görebilirsiniz. Ürünlerinizi hangi ülkeler hangi ülkelerden satın alıyor, tüm detayları görebilirsiniz. Ürününüz ile ilgili pazarları inceleyebilir, ithalat hacimlerini yorumlayabilirsiniz.',
    'mainpage_es_four_title'=>'Veri Madenciliği Geleceğinizi Kurtarır',
    //'mainpage_es_four_desc'=>'Tüm potansiyel müşteri verilerinizi dijitalde depolamanız bu müşterilere erişiminizi kolaylaştırır. Ürünlerinize göre web sitenizdeki içeriklere göre segmentler oluşturulur ve ziyaretçilerinizin izinli verileri depolanır. Segmentleri 6 saatlik, günlük, aylık, 365 günlük gibi tüm zaman dilimlerinde ayırabilirsiniz.',
    'mainpage_es_four_desc'=>'Tüm potansiyel müşteri verilerinizi dijitalde depolamanız bu müşterilere erişiminizi kolaylaştırır. Ürünlerinize ve web sitenizdeki içeriklere göre segmentler oluşturulur ve ziyaretçilerinizin izinli verileri depolanır. Segmentleri 6 saatlik, günlük, aylık, 365 günlük gibi tüm zaman dilimlerinde ayırabilirsiniz.',
    'mainpage_es_five_title'=>'Markanızı Korumak Ticaretinizi Korur',
    'mainpage_es_five_desc'=>'Seçtiğiniz piyasalara ihracat yaparken en önemli adım marka tescilinizin olmasını sağlamaktır. Bu piyasalardaki marka tescillerinizin uygunluğunun araştırılması ve sonrasında tescil edilmesini sağlıyoruz.',
    'mainpage_es_six_title'=>'Sonuç : Müşteri Eşleştirme ile İhracat Artışı',
    'mainpage_es_six_desc'=>'Ürünlerinizi farklı pazarlardan, farklı firmalardan alan alıcılarla sizleri dijital kanallar üzerinden eşleştiriyoruz. Datanın, veri madenciliğinin değerini sonuna kadar kullanıyorsunuz ve doğru MÜŞTERİ’nin karşısına sürekli siz çıkıyorsunuz. Müşteri verileri ile ihracatınızı geliştiriyorsunuz.',

    /*mainpage  finish */

    /*footer  start */

    'socialmedia_accounts' => 'Sosyal medya hesaplarımız',
    'youcan_followus' => 'Sosyal Medya hesaplarımızdan bizi takip edebilirsiniz.',
    
    /*footer  finish */

    /*aplication form  start */
    'application_form' => 'Başvuru Formu',
    'application_form_content' => 'Aşağıdaki formu eksiksiz doldurarak sisteme kayıt işleminizi başlatabilirsiniz.',
    'personal_information' => 'Kişisel Bilgiler',
    'company_information' => 'Şirket Bilgileri',
    'productname_or_code' => 'Ürün Adı veya Kodu',
    /*aplication form  finish */

    /*data yönetimi  start */
    'data_pageheader_title' => 'E-ihracatı kendi datanızı kullanarak artırın.',
    'data_pageheader_content' => 'Her e-ticaret sitesi yurt dışına satış yapabilir. Önemli olan yurt dışı satışlarınızı artırmak için ziyaretçileri tanımak ve ihtiyaç duydukları anda doğru ürünü karşılarına çıkarmak. Web sitenize gelen ziyaretçileri davranışlarına göre detaylı şekilde analiz edin. E-ihracatınızı artırmak için ziyaretçileriniz tanıyın, web sitenizdeki davranışlarına bağlı detaylı segmentler oluşturun ve yeniden hedefleyerek online’da ürünlerinizi tekrar hatırlatın.',
    'data_whatisthis' => 'NEDİR NE İŞE YARAR ?',
    'data_whatisthis_title' => 'İhracat yapmak istediğiniz ülkelere özel segmentler.
    ',
    'data_whatisthis_content_one' => 'Ürünlerinizi ulaştırmak istediğiniz ülkelerde yaptığınız tanıtım ve reklamlarla web sitenize gelen ziyaretçileri müşteriye dönüştürün. Coğrafi segmentler ile izinli ziyaretçilerinizin datalarını ülke bazında ayrıştırın.',
    'data_pageheader_content_two' => 'Örneğin ; web sitenize Almanya’dan gelen ziyaretçileri, İngiltere’den gelen ziyaretçileri ayrı ayrı hedeflenebilir data segmentlerinde gruplayın. Her ülkeye özel potansiyel müşterileri yeni müşteriye dönüştürebilecek yeniden hedefleme kampanyaları oluşturun.',
    'main_features' => 'ANA ÖZELLİKLER',
    'data_mainfeatures_title' => 'Web sitenize gelen ziyaretçileri davranışlarına göre ayrıştırın.',
    'data_mainfeatures_content' => 'Web sitenize gelen her ziyaretçi sizin için potansiyel müşteri demektir. Uzun tanıtım uğraşları ile sitenize getirdiğiniz ziyaretçileri yeni müşteriye dönüştürmek için ilgilendikleri ve ilgilenebilecekleri ürünleri gösterin.',

    'data_rules_content_one' => ' Ülke bazlı segmentler ile hedef pazarınızdan sitenize gelen ziyaretçilerin hangi ülkeden geldiğini belirleyin ve ziyaretçilerin dilinde özel reklamlar yapın.',
    'data_rules_content_two' => 'Ziyaretçilerinizin ürün inceleme, sepete ürün ekleme, bir ürünü birden çok ziyaret etme, ürün sayfasında geçirdikleri süre gibi davranışlarını izleyerek tanıyın ve ihtiyaçlarını analiz edin.',
    'data_rules_content_three' => 'Web sitenize ilk defa gelen ziyaretçileri kaybetmeyin, yeni müşteriye dönüştürün. Müşterilerinizi de davranış ve ihtiyaçlarına göre segmente ederek satın alma oranlarını artırın.',
    'data_rules_content_four' => 'Sizden daha önce ürün alan müşterilerinizin daha önce aldıkları ürünleri inceleyerek gelecekte ihtiyaç duyabilecekleri ürünlerin reklamlarını gösterin.',
    'data_rules_content_five' => 'Hedeflediğiniz pazarlarda marka bilinirliğini artırırken satışlarınızı da data yönetimi ve data hedefli reklamlar ile artırın.',
    'data_rules_content_six' => 'Data segmentlerini oluştururken ziyaretçilerin kullandığı cihaz, ziyaret ettikleri ya da sepete attıkları ürünler, sitenize hangi kaynaktan geldiği, hangi sayfayı kaç defa ziyaret ettiği gibi birçok kuralı değerlendirerek hedef kitlenizi oluşturun.',
    'data_rules_content_seven' => 'Ürünleriniz ile online’da etkileşim kuran kişileri bulundukları ülkelerde, yerelleştirilmiş reklamlar ile hedefleyin. Potansiyel müşterileriniz ile anlayabilecekleri ve satın almaya teşvik edecek şekilde iletişim kurun.',
    'data_rules_content_eight' => 'XML feedler ile ürünlerinizi yükleyin ve her bir kategori ve ürün için oluşturulacak otomatik segmentleri kampanyalarınızda istediğiniz gibi kullanın.',
    'data_rules_content_nine' => 'THEAD DSP entegrasyonu segmentlerinizi kolayca DSP platforma aktarın ve retargeting kampanyalarını hızlıca oluşturun.',

    'additional_features'=>'EK ÖZELLİKLER',
    'date_features_title_one'=>'Teknoloji Danışmanlarımız Sürekli Sizinle',
    'date_features_content_one'=>'Ziyaretçilerinizi tanımanız ve analiz etmeniz için teknoloji danışmanlarımız sürekli sizinle. E-ihracatı artırmak için hedeflediğiniz pazarlara özel segmentler ile hedeflenebilir kitleleri kendi ziyaretçilerinizden oluşturuyor, yeni müşteriler edinmenizi sağlıyoruz.',
    'date_features_title_two'=>'“YENİ NORMAL”de e-ihracat',
    'date_features_content_two'=>'Yeni normalde ziyaretçiler online alışverişi daha yoğun kullanıyor. Bu durum yurt dışından web sitenize gelecek ziyaretçi sayısını artıracaktır. Gelen ziyaretçileri analiz ederek gelişmiş remarketing planları ile potansiyel müşterilerinizi belirliyor ve ihracat yapmak istediğiniz ülkelerde bu kişilere erişmenizi sağlıyoruz.',
    'date_features_title_three'=>'Çapraz Satış İçin Datanın Gücü',
    'date_features_content_three'=>'Sadece coğrafi segmentler değil ürün bazlı detaylı segmentler ile ziyaretçilerin ilgilendikleri ürünleri belirliyor, bu ürünleri alan kişilere satın alabilecekleri diğer ürünlerinizin gösterilmesi sağlıyoruz. Online’da çapraz satış politikanızı geliştirecek planları size özel hazırlıyoruz.',
    'date_features_title_four'=>'Lojistik ve Kargo İşlemlerini Düşünmeyin',
    'date_features_content_four'=>'Dünyada her yere erişimi olan DHL gibi büyük kargo firmaları ile yaptığımız anlaşmalar sayesinde, ürünlerinizi dünyanın dört bir yanına eriştirmenizi sağlıyoruz. Kargo işlemleri için yeni hedef pazarınıza uygun firmaları araştırıyor ve lojistik sürecinizi kolaylaştırıyoruz.',
    
    /*data yönetimi  finish */

    /*marketing management  start */
    'emarketing_pageheader_title' => 'Ürünlerinizi global pazaryerlerine taşıyın.',
    'emarketing_pageheader_content' => 'Ürünlerinizi yerel pazar yerlerine eklerken aynı zamanda e-ihracatınızı geliştirmek için global pazaryerlerine de ekleyin. Baştan sona tüm pazar yeri yönetiminizi gerçekleştirelim. Hem yurtiçi hem de global satışlarınızı birlikte artıralım.',

    'emarketing_whatisthis_title' => 'Ürünlerinizi dünyanın dört bir yanına ulaştırın.',
    'emarketing_whatisthis_content_one' => 'E-ihracatınızı global pazar yerlerinde yer alarak, ürünlerinizi hedef pazarlarda tanıtarak geliştiriyoruz. Amazon, eBay, Aliexpress gibi dünyanın en büyük pazar yerlerinde mağaza aktivasyonu ve ürün yönetimi süreçlerinde yanınızda oluyoruz',
    'emarketing_whatisthis_content_two' => 'E-ticaret ve e-ihracat trendlerini sizin için takip ediyor, pazar yerlerinde yeni e-ihracat dinamiklerine uygun hareket ederek, global pazarlarda yeni müşteri edinmenizi ve satış hedeflerinize ulaşmanızı sağlıyoruz.',
    'emarketing_mainfeatures_title' => 'Dijitalde Ürün Satışlarını Artırmanın En Etkili Yolu',
    'emarketing_mainfeatures_content' => 'Ürün satışlarınızı hem yurt içi pazarda hem de hedeflediğiniz ülkelerdeki global ve yerel pazarlarda artırmanın en etkili yolu, hali hazırda hedef kitlenin aşina olduğu pazar yerlerinde yer almak ve ürünlerinizi tanıtmaktır.',
    'emarketing_mainfeatures_content_two'=>'Hepsiburada, Gittigidiyor, Trendyol, N11 gibi Türkiye’nin önde gelen pazar yerlerinde yer almanızı sağlayarak tüm mağaza yönetim süreçlerini gerçekleştiriyoruz. Aynı zamanda e-ihracatınızı geliştirmeniz için Amazon, eBay, Aliexpress gibi önde gelen global pazar yerleri için mağaza başvurularınızı, ürün ve gönderi yönetim süreçlerinizi üstleniyoruz.',

    'emarketing_rules_content_one'=>'Tüm yerel ve global pazar yerlerine entegrasyon, mağaza açma, ürün yönetimi ve satış odaklı kampanya yönetimini gerçekleştiriyoruz.',
    'emarketing_rules_content_two'=>'Hedef kitlenizi belirliyor, bu kitleye uygun yöntemler ile ürün/kitle etkileşimini artırıyor ve yeni müşteri edinmenizi sağlıyoruz.',
    'emarketing_rules_content_three'=>'Ürünleriniz için global pazar yerinde doğru analizler sağlayarak, hedef kitlenin ihtiyaçlarını ve satın alma eğilimlerini analiz ederek potansiyel müşteri kitlesine erişmenizi sağlıyoruz.',
    'emarketing_rules_content_four'=>'Ürünleriniz için doğru kampanya yönetimi ve hedef kitleniz için etkili pazarlama teknikleri ile satıcı ve müşteri arasındaki ilişkiyi güçlendiriyoruz.',
    'emarketing_rules_content_five'=>'Yurt dışı depo ihtiyacınız için çözümler sunuyor ve yurt dışı şirket kurulum işlemlerinizde uzman ekibimiz ile süreçlerinizi kolaylaştırıyoruz.',
    'emarketing_rules_content_six'=>'Detaylı raporlamalar ile yerel pazar yerlerinde e-ticarette katettiğimiz yolu ve global pazar yerlerinde e-ihracatta katettiğimiz yolu şeffaf şekilde sunuyoruz.',


    'emarketing_features_title_one'=>'Teknoloji Danışmanlarımız Sürekli Sizinle',
    'emarketing_features_content_one'=>'Pazar yeri yönetiminde sürecin en başından sonuna kadar tüm adımlarda uzman ekibimiz ile sürekli irtibat kurabilir, aklınıza takılan tüm soruları sorabilirsiniz. Detaylı planlamalar ile e-ihracat yolculuğunuzda nasıl bir yol izleyeceğimize, satışlarınızı nasıl artıracağınıza birlikte karar verelim.',
    'emarketing_features_title_two'=>'“YENİ NORMAL”de e-ihracat ',
    'emarketing_features_content_two'=>'Yeni normalde ihracat işlemlerinde de dijital yöntemler ağırlık kazanıyor. Siz de ihracatınızı dijitale taşıyarak sürece ayak uydurun. E-ihracatınızı büyütmek için ürünlerinizi potansiyel müşterilerin alışık olduğu pazar yerlerine taşıyın ve dünyaya açılmanın dijital yollarını keşfedin.',
    'emarketing_features_title_three'=>'Yeni Müşteriler Elde Edin',
    'emarketing_features_content_three'=>'Satışlarınızı yerel pazarla sınırlı tutmadan, dünyanın dört bir yanından yeni müşteri elde edin. Detaylı pazar analizleri ile hedeflediğiniz ülkelerin pazarlarını inceleyin, müşteri alışkanlıklarını ve satın alma eğilimlerini detaylandırın. Doğru hedef kitleye etkili pazarlama teknikleri ile erişerek yeni müşteriler elde edin.',
    'emarketing_features_title_four'=>'E-ihracat İçin Şartları Uygun Hale Getirin',
    'emarketing_features_content_four'=>'Global pazar yerlerinde mağaza açmanız ve e-ihracat yapabilmeniz için gerekli marka işlemleri, depo ve lojistik süreçler, şirket kurmak için gerekli adımlar ve maliyetler, ülkelerin vergi ve kanunlarına ilişkin bilmeniz gereken tüm detayları birlikte araştıralım. Başvurularınızı gerçekleştirelim ve ürün yönetimini aksatmadan yürütelim.',

    /*marketing management  finish */

      /*facebook-export  start */
      'facebook_pageheader_title' => '“Sosyal Medya ile firmanızı sınırların ötesine taşıyın”
      Sosyal Medya ile dünyanın dört bir yanından
      yeni müşteriler edinin.',
      'facebook_pageheader_content' => 'Potansiyel müşterileriniz nerede olursa olsun Facebook ve Instagramda mutlaka zaman geçiriyordur. Sosyal Medya ile ihracatınızı geliştirmeniz için hedef kitle istatistikleri ve “Sınır Ötesi istatistik bulucu” ile ürünlerinize en uygun kitleyi belirliyoruz.',

      'facebook_whatisthis_title' => 'Sosyal Medyada Ürün-Hedef Kitle İlişkisi',
      'facebook_whatisthis_content_one' => 'İster dünya genelinde isterseniz belirlediğiniz hedef ülkelerde ürünlerinizle ilişkili potansiyel müşteri kitleleri oluşturuyoruz. Ürünlerinize ve dönüşüm, trafik, video izlenme gibi kampanya hedeflerinize uygun özel hedef kitlelere ya da farklı ülkelerde kullanabileceğiniz benzer hedef kitlelere erişim sağlıyoruz.',
      'facebook_whatisthis_content_two' => 'Facebook ve Instagram’da birden fazla dilde yayınlanan reklamlarınızın ürünlerinizle en alakalı kişilere gösterilmesini sağlıyoruz.',
      'facebook_mainfeatures_title' => 'E-ihracatınızı sosyal medyada sınır ötesine taşıyın',
      'facebook_mainfeatures_content' => 'Sosyal medya kullanıcıları içerisindeki ürününüzle ilişkili potansiyel müşterilerinizi hedefleyerek ihracatınızı büyütebilirsiniz. Potansiyel müşterilerin birçoğu Facebook ve Instagram’da çokça vakit geçiriyor. Web sitenizde hizmet verdiğiniz tüm diller için farklı kampanyalar ile hedef kitleye uygun sosyal medya reklamları oluşturuyoruz.',

      'facebook_rules_content_one' => 'İstatistik bulucu ile sektörünüze uygun hedef kitleyi bulunduğunuz mevcut pazarda oluşturuyor ve bu kitleye uygun hedef pazarınızda benzer kitleler oluşturuyoruz.',
      'facebook_rules_content_two' => 'İsterseniz pazara açılmayı düşündüğünüz ülke için özel kitleler oluşturarak sizin belirlediğiniz kriterlerdeki kişilere de erişebilirsiniz. Ayrıca web sitesi ziyaretçilerini ya da mobil uygulama kullanıcılarını da hedefleyebilirsiniz.',
      'facebook_rules_content_three' => 'Facebook ve Instagram’da potansiyel müşterilerin konumuna göre, bulunduğu ülke ya da şehre göre reklamlarınızı planlayabilirsiniz.',
      'facebook_rules_content_four' => 'Sosyal medya üzerinden ürününüzü potansiyel müşteriler ile eşleştiriyor, sınır ötesi satışlarınızı artırmanızda yardımcı oluyoruz. ',
      'facebook_rules_content_five' => 'Hedeflediğiniz her bir ülkedeki müşterilere yönelik hedef kitleye uygun yerel dillerde reklamlarınızı oluşturuyoruz.',
      'facebook_rules_content_six' => 'Sosyal medya üzerinde e-ihracatınızı büyütme ve geliştirme sürecinde uzman ekibimiz ile medya planınızı ve pazarlama stratejinizi oluşturuyor, kampanya yönetimi ve tüm raporlama süreçlerinde yanınızda oluyoruz. ',
      
      'fb_features_title_one' => 'Teknoloji Danışmanlarımız Sürekli Sizinle',
      'fb_features_content_one' => 'Ziyaretçilerinizi tanımanız ve analiz etmeniz için teknoloji danışmanlarımız sürekli sizinle. E-ihracatı artırmak için hedeflediğiniz pazarlara özel segmentler ile hedeflenebilir kitleleri kendi ziyaretçilerinizden oluşturuyor, yeni müşteriler edinmenizi sağlıyoruz.',
      'fb_features_title_two' => 'CRM Entegrasyonu',
      'fb_features_content_two' => 'Yeni normalde ziyaretçiler online alışverişi daha yoğun kullanıyor. Bu durum yurtdışından web sitenize gelen ziyaretçi sayısını artıracaktır. Gelen ziyaretçileri analiz ederek gelişmiş remarketing planları ile potansiyel müşterilerinizi belirliyor ve ihracat yapmak istediğiniz ülkelerde bu kişilere erişmenizi sağlıyoruz.',
      'fb_features_title_three' => 'Sosyal Medyada Yeniden Hedefleme',
      'fb_features_content_three' => 'Sadece coğrafi segmentler değil ürün balı detaylı segmentler ile ziyaretçilerin ilgilendikleri ürünleri belirliyor, bu ürünleri alan kişilere satın alabilecekleri diğer ürünlerinizin gösterilmesi sağlıyoruz. Onlineda çapraz satış politikanızı geliştirecek planları size özel hazırlıyoruz.',
      'fb_features_title_four' => 'Devlet Desteklerinden Yararlanın',
      'fb_features_content_four' => 'Dünyada her yere erişimi olan DHL gibi büyük kargo firmaları ile yaptığımız anlaşmalar sayesinde, ürünlerinizi dünyanın dört bir yanına eriştirmenizi sağlıyoruz. Kargo işlemleri için yeni hedef pazarınıza uygun firmaları araştırıyor ve lojistik sürecini kolaylaştırıyoruz.',
      
      /*facebook-export  finish */

      /*google_marketfinder  start */
      'gm_pageheader_title' => 'Ürünlerinizi Google Marketfinder ile tüm dünyaya satın.',
      'gm_pageheader_content' => 'Google Marketfinder ile tüm dünyadan yeni müşterilere ürünlerinizi tanıtın ve yeni pazarlara açılın. Dünya çapında ürünlerinizi arayan potansiyel müşterileri inceleyin ve ihracat planınızı oluşturun.',

      'gm_whatisthis_title' => 'Web sitenize uygun pazar önerileri',
      'gm_whatisthis_content_one' => 'Web sitenizde bulunan ürün kategorilerine uygun, ülkelerin online satın alma davranışlarının sonucunda oluşan pazar önerilerini sunuyoruz. Birlikte ürün kategorilerinizi inceliyor ve Google Marketfinder ihracat aracı ile detaylı piyasa araştırmaları yapıyoruz',
      'gm_whatisthis_content_two' => 'Hedef ülkelerde sattığınız ürünleri arayan kişi sayılarını, ülkelerin ekonomik durumlarını, ihracata uygun hale gelmeniz için gerekli kanunları ve daha birçok veriyi inceliyor ve sizin için en uygun pazarları belirliyoruz.',
      'gm_mainfeatures_title' => 'Online arama ve satın alma verisi odaklı ihracat planlama',
      'gm_mainfeatures_content' => 'Sattığınız ürün kategorileri neler, bu kategorilerde dünya çapında yapılan Google aramaları ne kadar, hedef ülkelerinizdeki kişilerin satın alma eğilimleri ve gelirleri nasıl, lojistik ve kanuni zorlukları var mı gibi birçok soruyu birlikte cevaplıyoruz',
      'gm_mainfeatures_content_two' => 'Uygun pazarları tespit ettikten sonra web sitenizde yapmanız gereken çalışmalarınızı, e-ihracatınızı geliştirecek ve ürün satışınızı artıracak reklam planlarınızı oluşturuyoruz.',



      'gm_rules_content_one' => 'Web sitenizi tarıyor ve ürün kategorilerinizi belirliyoruz. İsterseniz ek kategoriler de ekleyebilirsiniz.',
      'gm_rules_content_two' => 'Sizin belirlediğiniz kategoriler için online arma verilerine göre en uygun pazarları belirliyoruz. Bu pazarlarda yapacağınız tanıtımlar için Google ads tekliflerini inceliyoruz.',
      'gm_rules_content_three' => 'Ülke profilleri ve ülkelerin ekonomik profillerini inceleyerek burada yaşayan kişilerin satın alma eğilimleri ve ekonomik durumları hakkında detaylı bilgiler sunuyoruz.',
      'gm_rules_content_four' => 'Firmaların zorluk yaşadığı web sitesi yerelleştirme, lojistik, uluslararası ödeme gibi birçok hususta detaylı kılavuzlar ile yardımcı oluyoruz.',
      'gm_rules_content_five' => 'Google Ads reklamlarınız için ülke bazında detaylı medya planları oluşturuyor, hedeflemeniz gereken doğru kitleye ürünlerinizi tanıtmanızı sağlıyoruz. Pazarlama stratejinizi eksiksiz oluşturmanızda yardımcı oluyoruz.',
      'gm_rules_content_six' => 'Potansiyel müşterileriniz hangi cihazları kullanıyor, reklamlarınız için uygun dil hangisi, hangi reklam modelleri ile yeni müşterilere erişebilirsiniz gibi aklınıza takılan birçok soruyu yanıtlıyor, reklamlarınızın oluşturma ve yönetim süreçlerini takip ediyoruz.',

      
      'gm_features_title_one' => 'Web Sitesi Yerelleştirme',
      'gm_features_content_one' => 'Web sitenizde bulunan içerikleri sadece tercüme ettirmeniz hedef kitle tarafından anlaşılmanız için yeterli olmayabilir. Yanlış tercüme işlemleri işinizi daha da zorlaştırabilir. Reklamlarınızın ve web sitenizin anlaşılır olması için hedef ülkelerde yaşayan kişilerin kullandığı dillere uygun içeriklerinizi yerelleştirmeniz gerekir. Tüm içerik düzenleme süreçlerinizde de sizlere yardımcı oluyoruz.',
      'gm_features_title_two' => 'Detaylı Medya Planı ve Reklam Yönetimi',
      'gm_features_content_two' => 'Google Marketfinder ile gerçekleştireceğimiz analiz sonrasında Google Ads reklamları için detaylı bir medya planı hazırlıyoruz. Hangi reklam modellerini kullanmalısınız, potansiyel müşteriler hangi kelimeleri arıyor, hedef kitleye hangi dille erişmelisiniz gibi tüm detayları hazırlıyoruz. Google Ads reklamlarınızın oluşturma, yönetim ve raporlama süreçlerinde bütün yükü üstleniyor ve e-ihracat süreçlerinizin tamamında yanınızda oluyoruz.',
      'gm_features_title_three' => 'Uluslararası Ödemeler, Vergiler ve Hukuki Süreçler',
      'gm_features_content_three' => 'E-ihracat süreciniz sadece ürün tanıtımından oluşmuyor. Hedef ülkelerde sizi bekleyen ödeme, vergi, lojistik ve hukuki süreçler ile ilgili herhangi bir zorluk olup olmadığını detaylandırıyor ve raporluyoruz. Hedeflediğiniz ülkede ihracata uygun hale gelmeniz için gereken maliyetleri belirliyoruz.',
      'gm_features_title_four' => 'Devlet Desteklerinden Yararlanın',
      'gm_features_content_four' => 'Yurt dışında ürünlerin reklamını yaparak ihracat yapmayı ya da ihracatını büyütmeyi hedefleyen firmalara 250.000 USD’ye kadar verilen devlet desteklerinden yararlanın. Google Ads de devlet desteklerinden yararlanabileceğiniz platformlar arasında yer almaktadır.',

      

      /*google_marketfinder  finish */
      
      /*media_planning  start */
      'media_pageheader_title' => 'İhracatınızı güçlü bir medya planı ile geliştirin.',
      'media_pageheader_content' => 'Markanızı ve ürünlerinizi hedef pazarlarda tanıtmak, globalde potansiyel müşterileri belirlemek ve erişmek için çok yönlü, marka bilinirliği ve ihracat odaklı, size özel, güçlü bir medya planı hazırlıyoruz. Küresel pazarlarda ürün satışınızı artırmaya yönelik tüm reklam ve tanıtım faaliyetleri için detaylı planlar ile hedeflerinize ulaşmanızda yardımcı oluyoruz.',
      'media_whatisthis_title'=>'Potansiyel MÜŞTERİ’yi hedefleyin',
      'media_whatisthis_content'=>'Ürünleriniz ile ilgilenebilecek potansiyel müşteri kitlesini hedefleyin. Yurt dışında sektörel bazlı ya da dikey data bazlı istediğiniz hedef kitleyi belirleyelim ve sizin istediğiniz web ve mobil sitelerde reklamlarınızın yayınlanmasını sağlayalım.',
      'media_whatisthis_content_two'=>'Büyük hedeflere giden yol büyük veriden geçer. Veri odaklı detaylı medya planı ile size özel ve hedef ülke bazında hedef kitle oluşturuyor ve ürünlerinizi ilgili kişilerin görmesini sağlıyoruz.',
      'media_mainfeatures_title'=>'Dijital dönüşüme ayak uyduran medya planlama',
      'media_mainfeatures_content'=>'Yeni normalde dijital dönüşüm ve bu dönüşüme uygun medya planlama daha güçlü bir ihtiyaç haline geldi. Potansiyel müşterileriniz ile dijital kanallarda etkileşim kurmanıza, pazarlama hedeflerinize uygun dijital medya planınızı detaylı şekilde oluşturmanıza ve yeni ziyaretçiler ile birlikte yeni müşteriler elde etmenize yardımcı oluyoruz.',
      'media_mainfeatures_content_two'=>'Firmanıza ve ürünlerinize özel kitle odaklı ve çok yönlü medya planını hazırlıyor, hem yurt dışındaki web sitelerinde hem de sosyal medyada reklamlarınızın hedef kitleye ulaştırılmasını sağlıyoruz.',
      'media_rules_content_one'=>'İhracat yapmayı hedeflediğiniz ülkelerde marka bilinirliğini sağlamanız ve yeni ziyaretçiler elde etmeniz için programatik reklam planınızı oluşturuyoruz. Markanızın görsel ya da video reklamlarını istediğiniz web ya da mobil sitelerde size uygun hedef kitlenin görmesini sağlıyoruz.',
      'media_rules_content_two'=>'Dünyanın ilk ve tek yapay zeka tabanlı ihracat planlama aracı THEADX EXPORT ile ülke bazında sizin ürünlerinizi satın alan firmaları tespit ediyor ve bu firmaların lokasyonlarında reklamlarınızın sürekli olarak gösterilmesini sağlıyoruz.',
      'media_rules_content_three'=>'İhracat yapmak istediğiniz ülke bazında, ürünlerinize özel hem sektörel hem de dikey kitle segmentasyonu ile rastgele reklam gösterimi yerine, satın alma olasılığı daha yüksek kişilere erişmenizi sağlıyoruz.',
      'media_rules_content_four'=>'Ziyaretçi analizi ile web sitenize gelen ziyaretçilerden spesifik hedef kitleler oluşturuyor ve bu kitlelere özel dinamik reklamlar ile inceledikleri ürünleri ve markanızı online’da sürekli olarak hatırlatıyoruz.',
      'media_rules_content_five'=>'İhracat yapmak istediğiniz her ülke için yerelleştirilmiş reklamlar ile hedef kitlenizin anlayacağı dilde etkileşim kurmanızı sağlıyoruz. Reklamlarınızı istediğiniz ülke ya da şehirde istediğiniz zamanda, doğru kişilere gösterilecek şekilde planlıyoruz.',
      'media_rules_content_six'=>'Programatik satın alma süreçlerini, kullanacağınız reklam modellerini, hangi mecrada ne kadar bütçe harcamanız gerektiğini, her mecra için hedef ve dönüşümlerini yani kısacası dijital kanallarda müşteriler ile ne şekilde etkileşim kurmanız gerektiğini sizin için araştırıyor, detaylandırıyor ve planlıyoruz.',
      'media_features_title_one'=>'Pazarlamanın Sürekliliği',
      'media_features_content_one'=>'Piyasa araştırması sürecinde sizin için uygun ülkeleri tespit ederken ya da hedeflediğiniz ülkenin ithalat hacmini incelerken teknoloji danışmanlarımız ile sürekli irtibat halinde olun. Pazar analizi ve medya planı işlemlerinizde uzman ekibimiz ile video konferanslar yapabilirsiniz.',
      'media_features_title_two'=>'Müşteri Odaklı Reklam Planı',
      'media_features_content_two'=>'Yapacağınız online tanıtımlar ile web sitenizi ziyaret eden, ürünlerinizi inceleyen ziyaretçilerin izinli verilerini dijitalde depolayın. Medya planınızda ziyaretçi verisi odaklı dinamik reklamlara da yer vererek ziyaretçilerinize kişiselleştirilmiş avantajlar sunun ve ürünlerinizi sürekli olarak hatırlatın.',
      'media_features_title_three'=>'Detaylı Raporlama ve Dönüşüm İzleme',
      'media_features_content_three'=>'Dijital medya pazarlama ve tanıtım işlemlerinizde yapacağınız harcamaların tüm detaylarını gelişmiş raporlarla sunuyor ve dönüşümleri takip ediyoruz. İhracat ve veri odaklı reklamlar ile dönüşüm oranınızı artırın.',
      'media_features_title_four'=>'Devlet Desteklerinden Yararlanın',
      'media_features_content_four'=>'Dünyada her yere erişimi olan DHL gibi büyük kargo firmaları ile yaptığımız anlaşmalar sayesinde, ürünleriniz dünyanın dört bir yanına eriştirmenizi sağlıyoruz. Kargo işlemleri için yeni hedef pazarınıza uygun firmaları araştırıyor ve lojistik sürecini kolaylaştırıyoruz.',
      

      /*media_planning  finish */

      
      /*online_sale_website  finish */
      'osw_pageheader_title'=>'Güçlü e-ticaret sitesi, Başarılı
      e-ihracat',
      'osw_pageheader_content'=>'E-ihracatınızı geliştirmek için yaptığınız tanıtım reklam faaliyetlerinin başarılı olması e-ticaret sitenizin ne kadar güçlü olduğuna bağlıdır. Hedeflediğiniz pazardaki kişilerin anlayabileceği şekilde yerelleştirilmiş, global ve ileri seviye modüller ile donatılmış, global pazar yerleri ile hızlı entegre edilebilir bir e-ticaret sitesi ile ürünlerinizi dünyanın dört bir yanına satın.',
      'osw_whatisthis_title'=>'Yurtdışına açılmak için global e-ticaret sitenizi hemen kurun',
      'osw_whatisthis_content'=>'Küresel satışa başlamak ve satışlarınızı artırmak için dünyanın her noktasından bağlanan kişilerin anlayabileceği profesyonel e-ticaret sitenizi kurarken her adımda yanınızda oluyoruz.',
      'osw_whatisthis_content_two'=>'Site kurulumu, hedef pazarlarınıza uygun dil desteği, yerelleştirme, lojistik desteği gibi birçok konuda düşünmenize bile gerek kalmadan sitenizi hızlıca faaliyete geçirmenizi sağlıyoruz.',
      
      'osw_mainfeatures_title' => 'Hedeflediğiniz küresel pazarlara hızlı ve sağlam adımlarla giriş yapın.',
      'osw_mainfeatures_content' => 'Sizin için en uygun tasarım ve alt yapı ile hızlı kurulum ve kolay kullanımlı e-ticaret siteniz ile e-ihracata güçlü bir başlangıç yapın. Dünyanın her yanına satış yapmanızı kolaylaştıracak tüm özellikleri bir arada sunuyor ve tek bir panelden tüm süreçleri yönetmenizi sağlıyoruz.',
      
      'osw_rules_content_one' => 'İster ulusal isterseniz uluslararası pazarlarda kullanabileceğiniz profesyonel modüller ve responsive tasarım ile her cihazda ziyaretçilerinize gelişmiş kullanıcı deneyimi sunmanızı sağlıyoruz.',
      'osw_rules_content_two' => 'Çoklu dil desteği ve yerelleştirme hizmeti ile ürünleriniz için hedeflediğiniz pazarlara uygun dil girişlerini kolayca yönetin. Yerelleştirme ile içeriklerinizi birebir tercüme etmeden dil girişlerini hedef kitlenizin anlayacağı şekilde gerçekleştirmenize yardımcı oluyoruz.  ',
      'osw_rules_content_three' => 'Küresel satışlarınızda ülke bazlı para birimi, gümrük ve vergi yönetimini profesyonelce gerçekleştirmenizi sağlıyoruz.',
      'osw_rules_content_four' => 'Ülke bazlı ürün ve fiyat yönetimi gerçekleştirebileceğiniz gelişmiş admin paneli sunuyoruz. Ürünlerinizi her ülke için farklı fiyatlandırabilir ve para birimini yönetebilirsiniz.',
      'osw_rules_content_five' => 'UPS , DHL gibi uluslararası kargo firmaları ile yaptığımız entegrasyonlarla dünyanın dört bir yanına ürün gönderme sürecini kolaylaştırıyoruz.',
      'osw_rules_content_six' => 'Lokasyon bazlı ziyaretçi tanıma özelliği ile ürünlerinizin, e-ticaret sitenize gelen ziyaretçinin bulunduğu ülkeye uygun dil ve para birimi ile gösterilmesini sağlıyoruz. Ziyaretçiyi müşteriye dönüştürmeniz için ürünlerinizin ilk açılışta kolayca anlaşılmasına yardımcı oluyoruz.',

      'osw_features_title_one' => 'Kolay Kurulum, Hızlı E-ihracat',
      'osw_features_content_one' => 'Teknoloji danışmanlarımız e-ticaret sitenizi ulusal ve global pazarda aktif ederken tüm adımlarda yanınızda. Site kurulumu, gelişmiş ve seo uyumlu tasarım, XML, API ve CSV dosyalar ile ürün yönetimi, dil, para birimi ve fiyat yönetimi gibi birçok süreci sizin için kolaylaştırıyoruz.',
      'osw_features_title_two' => 'Global Marketplace Entegrasyonları',
      'osw_features_content_two' => 'Ürünlerinizi hedeflediğiniz pazarlarda daha kolay satabilmek ve marka bilinirliğini artırmak için hedef kitlenin alışkın olduğu Ebay, Amazon, Aliexpress gibi bilinir pazar yerlerine ekleyin. E-ticaret sitenize yaptığımız entegrasyonlar ile küresel pazar yerlerine ürünlerinizi hızlıca ve kolayca ekleyin.',
      'osw_features_title_three' => 'Sanal Pos ve Ödeme Sistemi Yönetimi',
      'osw_features_content_three' => 'Hedef pazarlarınızda satış yapabilmeniz ve ödeme alabilmeniz için yurt dışından ödeme alabileceğiniz sanal pos yönetimi imkanı sunuyoruz. Ayrıca iyzico, PayU, Payguru, Paytrek gibi onlarca ödeme sistemi ile gerçekleştirdiğimiz entegrasyonlarla kolayca ödeme alabileceğiniz yöntemler de sunuyoruz.',
      'osw_features_title_four' => 'B2B ve B2C Satış Kolaylığı',
      'osw_features_content_four' => 'Hedef ülke bazında ürün, fiyatlandırma, vergi, gümrük ve para birimi yönetimi ile B2B ve B2C satışlarınızı kolaylaştırıyoruz. E-ihracat sürecini bir bütün olarak değerlendiriyor ve ürünlerinizi global pazarlarda satabilmeniz için sipariş, ödeme, lojistik gibi her hususta yanınızda oluyoruz.',
      

      
      /*online_sale_website  finish */


      /* theadx_export  start */
      //'adx_pageheader_title' => 'Theadx Export ile Tüm Dünyanın İhracatı Elinizin Altında',
      'adx_pageheader_title' => 'Yapay Zeka Destekli Export ile Tüm Dünyanın İhracatı Elinizin Altında',
      'adx_pageheader_content' => 'Dünyanın ilk yapay zeka tabanlı ihracat planlama aracı ile tüm dünyanın ihracat verilerini GTİP kodunuzu girerek yıl bazında tüm detayları ile inceleyin. Ürünleriniz ile ithalatçı firmaları eşleştirin.',
      'adx_whatisthis_title' => 'İhracat için ihtiyacınız olan tüm veriler.',
      'adx_whatisthis_content' => 'Ürününüzü hangi ülkeler satın alıyor, hangi ülkelerden daha çok alım yapılıyor, hangi yıllarda ihracat daha fazla ve ithalatçı ülkelerden hangi firmalar daha çok satın alım gerçekleştiriyor gibi aklınıza gelen birçok sorunun cevabını tek platformda bulmanız mümkün.
      ',
      'adx_whatisthis_content_two' => 'İhracat yaparken ya da ihracata başlarken gerek duyacağınız tüm verileri hizmetinize sunuyoruz.',
      'adx_mainfeatures_title' => 'Adım adım ihracatınızı geliştirin. Detaylı piyasa araştırması, veri madenciliği ve medya planlama.',
      'adx_mainfeatures_content' => 'GTİP kodunuzu girerek ürününüzün ülkelerdeki ticaret hacmini, ürün bazlı ticaret durumunu, yıllara bağlı ihracat büyüme oranlarını inceleyin. Hatta belirli ülkelerde ürününüzün ihracat durumunu inceleyebilir ve detaylı piyasa analizi yapabilirsiniz. İstediğiniz ülkelerde ithalatçı firmaları ürününüz ile eşleştirebilir ve satın alan firmaları inceleyebilirsiniz.',
      'adx_rules_content_one' => 'İster dünya çapında isterseniz ülke bazında son 30 yılın ihracat verilerini hizmetinize sunuyoruz. Açılmak istediğiniz pazarları ihracat öncesinde detaylı bir şekilde inceleyin',
      'adx_rules_content_two' => 'Dünya çapında ihracat işlemlerinde kullanılan GTİP kodlarına bağlı ürün satın alan firmaları tespit ettik. Ürününüzü hali hazırda diğer ülkelerden ya da başka ihracatçılardan satın alan potansiyel müşterileri inceleyin.',
      'adx_rules_content_three' => 'Hangi ülkede ya da firma lokasyonunda ne kadar gösterim yapmalısınız, ne kadarlık bütçe ayırmanız gerekli gibi soruları ortadan kaldırdık. İhracat hacmi olan ve ürün satın alan firmalara online’da erişmek için detaylı medya planını hazır olarak sizlere sunuyoruz.',
      'adx_rules_content_four' => 'Yapay zeka algoritmaları ile hedeflemeleri ithalatçı firmaların lokasyonlarına kadar sunuyoruz. Ürününüzü satın alacak firmaların lokasyonlarında sürekli reklamlarınızı gösteriyoruz.',
      'adx_rules_content_five' => 'Dünya genelinde 5.8 milyon firmaya nokta atışı ulaşmanızı ve ürünlerinizi doğru hedef kitleye, doğru şekilde göstermenizi sağlıyoruz',
      'adx_rules_content_six' => 'Ürünleriniz ile ilgili özel veri hizmeti sunuyoruz. Ürünlerinizi potansiyel müşteriler ile eşleştiriyor ve sizin için doğru hedef kitleyi belirliyoruz.',

      'adx_features_title_one' => 'Teknoloji Danışmanlarımız Sürekli Sizinle',
      'adx_features_content_one' => 'Piyasa araştırması sürecinde sizin için uygun ülkeleri tespit ederken ya da hedeflediğiniz ülkenin ithalat hacmini incelerken teknoloji danışmanlarımız ile sürekli irtibat halinde olun. Pazar analizi ve medya planı işlemlerinde uzman ekibimiz ile video konferanslar yapın.',
      'adx_features_title_two' => '“YENİ NORMAL”e Ayak Uydurun',
      'adx_features_content_two' => 'Yeni normalde yerinde Pazar analizi yapmak artık kolay olmayacak. Piyasa araştırmalarınızı online’da yaparak hem maliyetinizi düşürün hem de kapsamlı analizler gerçekleştirin. Sadece belirli ülkeleri değil tüm dünyanın ihracat verilerini tek ekrandan inceleyin. ',
      'adx_features_title_three' => 'Verilerinizi Dijitalde Depolayın',
      'adx_features_content_three' => 'Yapacağınız online tanıtımlar ile web sitenizi ziyaret eden, ürünlerinizi inceleyen ziyaretçileri online ortamda depolayın. Tanıtımlarınıza ilgi gösteren kişileri yeniden hedefleyerek ürünlerinizi sürekli hatırlatın',
      'adx_features_title_four' => 'Devlet Desteklerinden Yararlanın',
      'adx_features_content_four' => 'Yurt dışında ürünlerin reklamını yaparak ihracat yapmayı ya da ihracatını büyütmeyi hedefleyen firmalara verilen 250.000 USD’ye kadar devlet desteklerinden yararlanın. THEADX devlet desteklerinden yararlanabileceğiniz platformlar arasında yer almaktadır.',
      
      

      /*theadx_export  finish */


      /*together_social  start */
      //'ts_pageheader_title' => 'Markanızı hedef pazarlarda trend belirleyiciler ile bir adım öteye taşıyın',
      'ts_pageheader_title' => 'Markanızı hedef pazarlarda influencerlar ve içerik dağıtım networkleri ile bir adım öteye taşıyın',
      //'ts_pageheader_content' => 'Hedeflediğiniz ülkelerin pazarlarında marka ve ürün tanıtımı için sadece reklamlar ya da fuarlar gibi etkinlikler ile yetinmeyin. Ürünlerinizi ihraç etmek istediğiniz ülkelerde içerik dağıtımı ile reklam algısı yaratmadan bloglarda, web sitelerinde, sosyal medyada tanınmış kişilerle yapacağınız etkili pazarlama teknikleri ile tanıtın. ',
      'ts_pageheader_content' => 'Hedeflediğiniz ülkelerin pazarlarında marka ve ürün tanıtımı için sadece reklamlar ya da fuarlar gibi etkinlikler ile yetinmeyin. Ürünlerinizi ihraç etmek istediğiniz ülkelerde influencer pazarlaması ve içerik dağıtımı ile reklam algısı yaratmadan bloglarda, web sitelerinde, sosyal medyada tanınmış kişilerle yapacağınız etkili pazarlama teknikleri ile tanıtın.',
      'ts_whatisthis_content' => 'Hedeflediğiniz ülkelerde sosyal medyada, dünyanın her noktasında farklı kitlelere ürünlerinizi ulaştırın. Global influencer ağımız ile sosyal medya reklamlarınızı basit bir paylaşımın dışına çıkarıyor, yaratıcı ve dikkat çekici paylaşımlar ile influencer pazarlama sürecini en kaliteli hali ile deneyimlemenizi sağlıyoruz.',
      'ts_whatisthis_content_two' => 'Akılda kalıcı içerikler ile hedef kitlenize takip ettikleri, sevdikleri ve güven duydukları kişiler üzerinden daha kolay erişin.',


      'ts_mainfeatures_title' => 'İhracat yapmak istediğiniz ülkelerde ürünlerinizi doğal yollarla hedef kitleye tanıtın.',
      'ts_mainfeatures_content' => 'Global içerik dağıtım ağımız ile reklamın yanı sıra hedef kitlenizin ürünlerinizi doğal yollarla tanıyacağı yenilikçi pazarlama teknikleri uyguluyor, hedeflediğiniz ülkeler için size özel detaylı planlamalar ile doğru kitleye etkili yöntemler ile ulaşmanızı sağlıyoruz.',


      'ts_rules_content_one' => 'Globalde benzersiz hedef kitleler ile sosyal medyada ya da içerik dağıtım ağımızda ürünleriniz ile ilgili içerikleri farklı kişilerin görmesini sağlıyoruz.',
      'ts_rules_content_two' => 'Facebook, Instagram, LinkedIn gibi sosyal medya platformlarındaki sayfalarınızın içeriklerinin daha çok kişiye ulaşmasını sağlıyor, etkileşimi artırıyoruz.',
      'ts_rules_content_three' => 'Web sitenizde hedef kitle tarafından görüntülenmesini istediğiniz içeriklerinizi ya da ürün sayfalarınızı global içerik dağıtım ağımız ile daha çok kişiye ulaştırıyor ve marka bilinirliğini doğal yollarla artırmanıza yardımcı oluyoruz.',
      'ts_rules_content_four' => 'Ürünlerinizin ya da markanızın video tanıtımlarını istediğiniz ülkede daha fazla kişi tarafından izlenmesini sağlıyoruz. ',
      'ts_rules_content_five' => 'Hedeflediğiniz ülkede sosyal medyada tanınmış influencerlar ile yenilikçi ve yaratıcı içerikler üretiyor, ürünlerinizi dünyanın dört bir yanında tanıtmanıza yardımcı oluyoruz. ',
      'ts_rules_content_six' => 'Hedeflediğiniz her ülke için içerik dağıtım ağımızda bulunan etkili pazarlama teknikleri ile detaylı planlar hazırlıyor, her adımı en ince ayrıntısına kadar değerlendiriyor ve raporluyoruz.',

      'ts_features_title_one' => 'Hedef Pazarlarda Benzersiz Kitle Analizi',
      'ts_features_content_one' => 'İhracat yapmak istediğiniz ülkelerdeki ürün tanıtımı, video izlenme, ürünleriniz ile ilgili içeriklerin dağıtılması sürecinde her yöntem için benzersiz hedef kitleler oluşturuyor, ürünlerinizin daha fazla tekil kişi tarafından görüntülenmesini sağlıyoruz. ',
      'ts_features_title_two' => 'Sosyal Medyanın Gücünü İhracatta Kullanın',
      'ts_features_content_two' => 'Sosyal medyada basit paylaşımların dışına çıkın. E-ihracatın vazgeçilmezi olan ürün ve marka tanıtımı süreçlerinizde, sosyal medya sayfalarınızı, istediğiniz ülkelerdeki çok okunan blog sayfalarını, haber sitelerini ve sosyal medyada tanınmış fenomenleri pazarlama planınıza dahil ediyor ve e-ihracatınızı geliştiriyoruz.',
      'ts_features_title_three' => 'Daha Fazla Etkileşim Daha Çok Satın Alma',
      'ts_features_content_three' => 'Ürünlerinizin hedef kitle ile reklam algısı dışında, daha çok etkileşim kurmasını sağlıyoruz. Klasik dijital pazarlama metotlarının dışına çıkıyor, ürününüz ile içeriği bütünleştirerek potansiyel müşterilere ulaştırıyoruz. ',
      'ts_features_title_four' => 'E-ihracatta Yenilikçi Pazarlama Yöntemleri ',
      'ts_features_content_four' => 'Yeni pazarlara yenilikçi yöntemler ile ulaşmanız için detaylı pazarlama planları hazırlıyoruz. Hedeflediğiniz her ülkede sosyal medya ve içerik dağıtımı hakkında kapsamlı araştırmalar yapıyor, size uygun yayıncı ve hedef kitleyi tespit ediyoruz.',
      
      /*together_social  finish */


      /*web_mobil_gelistirme  start */
      'wmd_pageheader_title' => 'İhracat odaklı web ve mobil uygulama',
      'wmd_pageheader_content' => 'İhracata başlamadan önce web sitenizi ve mobil uygulamanızı hedeflediğiniz pazarlardan gelecek ziyaretçilerin anlayacağı şekilde düzenlemenize yardımcı oluyoruz. Yapacağınız tanıtım faaliyetleri ve dijital pazarlama için ayırdığınız bütçenin boşa gitmemesi ve ziyaretçileri müşteriye dönüştürmek için anlaşılır, SEO uyumlu web ve mobil uygulamanızı geliştiriyoruz.',
      'wmd_whatisthis_title' => 'Başarılı ihracatın temelini birlikte atalım.',
      'wmd_whatisthis_content' => 'İhracat yapmak istediğiniz ülkelerden web sitenizi ziyaret eden kişiler sizi web sitenizde anlattığınız şekilde tanır. Ziyaretçileri hızlı, anlaşılır ve SEO uyumlu bir web sitesi ile karşılayın. Ülke bazında kullanıcı deneyimini kolaylaştırın.',
      'wmd_whatisthis_content_two' => 'İhracat yapmayı hedeflediğiniz ülkelerde web sitesi içerikleriniz nasıl olmalı, ürün isimleri hedef pazarda nasıl kullanıyor gibi içeriklerinizin yerelleştirilmesi ile ilgili süreçte yanınızda oluyoruz. Web ve mobil uygulamanızı, markanızı ve ürünlerinizi en doğru anlatacak şekilde geliştiriyoruz.',
      'wmd_mainfeatures_title' => 'Gelişmiş web ve mobil uygulama ile başarılı kullanıcı deneyimi',
      'wmd_mainfeatures_content' => 'Kullanıcı deneyimini ön planda tutan, ürünlerinizi en açık hali ile ortaya koyacak ve markanızı en profesyonel şekilde yansıtacak uygulamalarınızı birlikte geliştirelim. Hedef pazara yönelik lokal seo çalışmaları ile ihracat hedeflediğiniz ülkelerde markanızı bir adım öne taşıyalım.',
      'wmd_mainfeatures_content_two' => ' Belirli bütçeler ayırarak uzun uğraşlar ile elde ettiğiniz ziyaretçileri müşteriye dönüştürmek için profesyonel web ve mobil uygulamanızı geliştirelim.',
      
      'wmd_rules_content_one' => 'Web ve mobil uyumlu tasarımlar ile tüm cihaz ve çözünürlüklerde ürünlerinizi anlatın. Responsive tasarımlar ile ziyaretçiler hangi cihazdan bağlanırsa bağlansın içeriklerinizi ve ürünlerinizi düzgün şekilde görmelerini sağlıyoruz. ',
      'wmd_rules_content_two' => 'Ziyaretçileri kendi dillerinde karşılayın. Web sitenize Almanya’dan bağlanan kişileri Almanca içerikler ile karşılarken, Rusya’dan gelen ziyaretçileri Rusça içerikler ile karşılayın.',
      'wmd_rules_content_three' => 'İhracat yapmak istediğiniz ülkelerde anahtar kelime planlaması, arama hacimleri gibi sizi öne çıkaracak birçok detayı analiz ediyor ve uygulama içeriklerini oluştururken lokal SEO uyumlu olmasını sağlıyoruz.',
      'wmd_rules_content_four' => 'Kullanıcı deneyimini ön planda tutuyor, kaliteli ve hızlı altyapı ile ziyaretçiyi sıkmadan ve odağını kaybetmeden ürünleriniz hakkında bilgi almasını sağlıyoruz.',
      'wmd_rules_content_five' => 'IOS ve Android platformlarda çalışacak native ya da web tabanlı mobil uygulamanızı edinin. Kullanıcılarınızın mobil deneyimini de ön planda tutarak mobil uygulamanızı geliştiriyoruz. ',
      
      'wmd_features_title_one' => 'UI / UX Tasarım',
      'wmd_features_content_one' => 'Kullanıcı deneyimini ön planda tutan web ve mobil arayüz tasarımları, uygulamalarınız üzerinde kullanıcı deneyimini ön planda tutan geliştirmeler ile gelen ziyaretçilere kişiselleştirilmiş deneyimler sunmanıza yardımcı oluyor. Ziyaretçilerin web sitenizde ya da mobil uygulamanızda daha çok vakit geçirmelerini sağlıyoruz.',
      'wmd_features_title_two' => 'İhracat & Dijital Dönüşüm ',
      'wmd_features_content_two' => 'İhracat yapmayı hedeflediğiniz ülkelerde hedef kitlenize en doğru şekilde erişmeniz için dijital dönüşüm sürecinin uygulama geliştirme adımını sorunsuz şekilde gerçekleştirmenizi sağlıyoruz. Hedef kitle tarafından anlaşılır olmanızı, markanızla ve ürünlerinizle daha kolay etkileşim kurmaları için yardımcı oluyoruz.',
      'wmd_features_title_three' => 'Mobil Uygulama & Marka Aidiyeti ',
      'wmd_features_content_three' => 'Mobil uygulamalar ile kullanıcı kitlenizin size her an mobil cihazlardan ulaşabilmesini sağlıyoruz. İnternet kullanıcılarının kullanım alışkanlıklarının yoğunlukla mobil cihazlara kayması, mobil uygulama ihtiyacını güçlendiriyor. Sürekli ulaşılabilir olmanızı ve kullanıcılarınız ile ürünleriniz arasında bağ kurmanızı sağlıyoruz.',
      'wmd_features_title_four' => 'Profesyonel Ekip, Kaliteli Hizmet',
      'wmd_features_content_four' => 'Web ve mobil uygulamanızın planlama aşamasından, tasarım, geliştirme ve tüm test süreçlerini işlerinde uzman profesyonel ekibimiz ile gerçekleştiriyoruz. Her adımda sizinle irtibat halinde kalarak uygulamalarınızı isteğinize uygun ve en kaliteli şekilde oluşturulmasını sağlıyoruz.',
      

      /*web_mobil_gelistirme  finish */

      /*contactus  start */
      'contact_contactus' => 'Bize ulaşmak için formu doldurun.',
      'contact_name_surname' => 'Adınız ve Soyadınız',
      'contact_email' => 'E-Posta Adresiniz',
      'contact_howcanhelp' => 'Size Nasıl Yardımcı olabiliriz?',
      'contact_phone_number' => 'Cep Telefonu Numaranız',
      'send_message' => 'Mesaj Gönderin',
      'send' => 'Gönder',
      'send_form' => 'Formu Gönder',
      'cancel' => 'Vazgeç',
      'contact_title' => 'Ünvan',
      'contact_company_title'=>'Firma Ticari Unvan',
      'export_authority'=>'İhracat Yetkilisi',

      'contact_phone' => 'Telefon',
      'contact_company' => 'Şirket',
      'contact_form' => 'İletişim Formu',
      'contact_form_desc' => 'Aşağıdaki formu eksiksiz doldurarak sisteme kayıt işleminizi başlatabilirsiniz.',
      'message' => 'Mesajınız',
      'contact_info' => 'İletişim Bilgileri',
      'contact_address' => 'Adres',
      'email' => 'E-mail',
      /*contactus  finish */
      'getoffer' => 'Teklif Al',
      'all_rights_reserved' => 'Tüm Hakları Saklıdır',
      

      /*contactform  start */
      
      'write_name' => 'Adınızı ve soyadınızı yazınız',
      'invalid_email' => 'Geçersiz E-Posta',
      'message_notfound' => 'Mesaj bulunamadı',
      'contact_error_message' => 'Tarafımıza bilgi ulaştırılırken bir hata oldu.',
      'please_email' => 'Lütfen e-posta gönderin.',
      'received_your_info' => 'Bilgileriniz bize ulaştı.',
      'get_back_soon' => 'size en kısa sürede geri dönüş sağlayacağız.',

      /*contactform  finish */


       /*export consult  start */
       'team' => 'Takım',
       'team_pageheader_title' => 'Takımın üyeleriyle tanışın. ',
       'team_synergy' => 'Ürününüz ya da hizmetiniz, bu Takımın sinerjisiyle başarıya ulaşıyor.',
       'team_member_name_one' => 'Emrah Pamuk',
       'team_member_title_one' => 'Co-Founder & CEO',
       'team_member_desc_one' => 'Pazarlama ve reklamcılık sektöründe 13 yılı aşkın süredir kendini ispatlar bir çalışma geçmişine sahip deneyimli bir kurucu ortak. Yüksek iletişim ve insan yönetimi becerilerine, dijital ve çevrimdışı pazarlama yönetimi konusunda stratejik bir yürütme uzmanlığına sahip, güçlü, kararlı, sonuç odaklı, tutkulu bir iş lideri…',
       'team_member_desc_two'=>'Dijitalde Kalın',


       'team_member_name_two' => 'Handan KÜTÜK',
       'team_member_title_two' => 'Business Development Director',
       'team_membertwo_desc_one' => '',
       'team_membertwo_desc_two'=>'',

       'team_member_name_three' => 'Rinet HOŞOL',
       'team_member_title_three' => 'Head of Programmatic & Facebook',
       'team_memberthree_desc_one' => '',
       'team_memberthree_desc_two'=>'',
       
       'team_member_name_four' => 'Barış YILMAZ',
       'team_member_title_four' => 'Media Purchasing Relations Director',
       'team_memberfour_desc_one' => '',
       'team_memberfour_desc_two'=>'',

       'team_member_name_five' => 'Ozan ÇAKMAK',
       'team_member_title_five' => 'Social Media Manager',
       'team_memberfive_desc_one' => '',
       'team_memberfive_desc_two'=>'',

       'team_member_name_six' => 'Dilara DAĞISTANLI',
       'team_member_title_six' => 'Business Development Specialist',
       'team_membersix_desc_one' => '',
       'team_membersix_desc_two'=>'',

       'team_member_name_seven' => 'Ceren MUSLU',
       'team_member_title_seven' => 'Business Development Specialist',
       'team_memberseven_desc_one' => '',
       'team_memberseven_desc_two'=>'',

       'team_member_name_eight' => 'Erman PAMUK',
       'team_member_title_eight' => 'Art Director',
       'team_membereight_desc_one' => '',
       'team_membereight_desc_two'=>'',

       'team_member_name_nine' => 'Serra ÇETİNKAYA',
       'team_member_title_nine' => 'Social Advertising Specialist',
       'team_membernine_desc_one' => '',
       'team_membernine_desc_two'=>'',

       'team_member_name_ten' => 'Mısra VOLKAN',
       'team_member_title_ten' => 'Brand Manager',
       'team_memberten_desc_one' => '',
       'team_memberten_desc_two'=>'',

       'team_member_name_eleven' => 'Dilge ÇETİNÇAY',
       'team_member_title_eleven' => 'Content Manager',
       'team_membereleven_desc_one' => '',
       'team_membereleven_desc_two'=>'',

       'team_member_name_twelve' => 'Kami GENÇBARONYAN',
       'team_member_title_twelve' => '',
       'team_membertwelve_desc_one' => '',
       'team_membertwelve_desc_two'=>'',

       'team_member_name_thirteen' => 'Efe ALTINELLİ',
       'team_member_title_thirteen' => 'E-commerce Performance Manager',
       'team_memberthirteen_desc_one' => '',
       'team_memberthirteen_desc_two'=>'',

       'team_member_name_fourteen' => 'Merve ANAÇ',
       'team_member_title_fourteen' => 'Performance Manager',
       'team_memberfourteen_desc_one' => '',
       'team_memberfourteen_desc_two'=>'',

       'team_member_name_fifteen' => 'Uğur AKÇIL',
       'team_member_title_fifteen' => 'Senior Lead Developer',
       'team_memberfifteen_desc_one' => '',
       'team_memberfifteen_desc_two'=>'',

       'team_member_name_sixteen' => 'Erol TUNA',
       'team_member_title_sixteen' => 'Frontend Developer',
       'team_membersixteen_desc_one' => '',
       'team_membersixteen_desc_two'=>'',

       'team_member_name_seventeen' => 'Efecan GÜLER',
       'team_member_title_seventeen' => 'Business Development Manager',
       'team_memberseventeen_desc_one' => '',
       'team_memberseventeen_desc_two'=>'',

       'team_member_name_eighteen' => 'Bahadır DİNDAR',
       'team_member_title_eighteen' => 'Sr. UX/UI Designer',
       'team_membereighteen_desc_one' => '',
       'team_membereighteen_desc_two'=>'',
       /*export consult  finish  */

      /*collobrators   start */
      'our_collaborators' => 'İşbirlikçilerimiz',
      'col_pageheader_title' => 'İşbirlikçilerimiz',
      'col_technology_partner_title' => 'Bizimle çalışan Müşterilerimiz',

      /*collobrators   finish  */

         /*partners   start */
         'partners_pageheader_title' => 'Partnerler',
         'technology_partners' => 'Teknoloji Partnerleri',
         'bussines_partners' => 'İş Partnerleri',
         'data_partners' => 'Data Partnerleri',
   
         /*partners   finish  */
);

?>