<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $lang['our_collaborators']; ?> - <?php echo $lang['ihracatin']; ?></title>
    <meta name="description" content="<?php echo $lang['metadesc_referances'] ?>" />
    <?php include 'includes/styles.php'; ?>
</head>
<body>
<?php include 'includes/header.php'; ?>
<section class="pageheader">
    <div class="page-header-content">
        <div class="page-header-content-box">
            <div class="container">
            <h1><?php echo $lang['col_pageheader_title']; ?></h1>
                <div class="page-header-menu">
                    <ul>
                        <li><a href="index.php"><?php echo $lang['mainpage']; ?>&nbsp;-&nbsp;</a></li>
                        <li><a class="active"><?php echo $lang['our_collaborators']; ?>&nbsp;</a></li>
                    </ul>
                </div>
               
            </div>
        </div>
        <div class="bottom"></div>
    </div>
    <img src="assets/images/pageheaders/collaborators.jpg" alt="" />

</section>
<section class="main-content">
    <div class="container">
        <div class="team-section partners">
            <div class="homeboxleft">
                <img src="assets/images/homepages/homeboxleft.png" />
            </div>
            <h2><?php echo $lang['col_technology_partner_title']; ?></h2>
            <div class="row">
                <div class="col-md-3">
                    <a href="https://www.adonis.com.tr/" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/collaborators/adonis.jpg" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://meysu.com.tr/" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/collaborators/meysu.jpg" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://www.dunyagoz.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/collaborators/dunyagoz.jpg" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://www.baruthotels.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/collaborators/barut.jpg" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://www.elaresort.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/collaborators/ela.jpg" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://www.akkahotels.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/collaborators/akka.jpg" alt="" /> 
                        </div>
                    </a>
                </div>

                <div class="col-md-3">
                    <a href="https://www.turkishairlines.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/collaborators/thy.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://www.emlakkonut.com.tr" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/collaborators/emlakkonut.jpg" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://www.aekhairclinic.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/collaborators/aekhairklinic.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://www.flowcoachinginternational.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/collaborators/flow_coaching.png" alt="" /> 
                        </div>
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="https://www.turkishsouq.com" target="_blank">
                        <div class="collaborator-box">
                            <img src="assets/images/collaborators/turkishsouq.jpg" alt="" /> 
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'includes/footer.php'; ?>

<?php include 'includes/scripts.php'; ?>
</body>
</html>
