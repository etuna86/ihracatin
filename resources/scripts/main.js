var hash = location.hash ;
$(document).ready(function() {
    $(".grayscale").hover(
        function () {
            $(this).addClass("grayscale-off");
        }, function () {
            $(this).removeClass("grayscale-off");
        }
    );

    console.warn("location.hash1; ",location.hash);
    if(hash!= undefined){
        $('html, body').animate({
            scrollTop:$(hash).offset().top -66,
        },1000 );
        if(hash=='#exporters'){
            var el = document.getElementById('nav-home-tab');
            el.click();
        }
        else if(hash=='#exporters2'){
            console.warn("location.hash; ",this.hash);
            var el = document.getElementById('nav-profile-tab');
            el.click();
        }
        else if(hash=='#exporters3'){
            var el = document.getElementById('eexporter-tab');
            el.click();
        }
        //window.location.hash = '';
    }else{
        console.warn($(hash).offset());
    }
    $(".list").on('click', function(event) {  
        event.preventDefault();
        var hash2 = this.hash ;
        console.warn("location.hash1; ",hash2);
        if(hash!= undefined){
            
            $('html, body').animate({
                scrollTop:$(hash2).offset().top -66,
            },1000 );
            if(hash2=='#exporters'){
                var el = document.getElementById('nav-home-tab');
                el.click();
            }
            else if(hash2=='#exporters2'){
                console.warn("location.hash; ",this.hash);
                var el = document.getElementById('nav-profile-tab');
                el.click();
            }
            else if(hash2=='#exporters3'){
                var el = document.getElementById('eexporter-tab');
                el.click();
            }
        }else{
            console.warn($(hash2).offset());
        }
    });
    

});

[].forEach.call(document.querySelectorAll('.toggleBodyNoScroll'), function(btn){
    btn.addEventListener('click', function() {
        document.body.classList.toggle('noscroll');
    })
})
  

$('.mobil-menu-btn').click(function () {
    $(this).toggleClass('opened');
    $('.mobile-menu').toggleClass('open');
})

$('.mobile-submenu-btn').click(function () {
    $(this).children('.triangle-block').toggleClass('opened-btn');
    $(this).children('.mobile-submenu').toggleClass('open-submenu');
})

$('.list').click(function () {
    $('.mobile-menu').removeClass('open');
    $('.mobil-menu-btn').removeClass('opened');
})


var a=0;
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll >= 200) {
        $( "header" ).addClass("header-white");
    } else {
        $( "header" ).removeClass("header-white");

    }

    if($('.counter-section').length > 0) {
        var oTop = $('.counter-section').offset().top - window.innerHeight;
        if (a == 0 && $(window).scrollTop() > oTop) {
            $('.counter-value').each(function() {
                var $this = $(this),
                    countTo = $this.attr('data-count');
                $({
                    countNum: $this.text()
                }).animate({
                        countNum: countTo
                    },
                    {
                        duration: 2000,
                        easing: 'swing',
                        step: function() {
                            $this.text(Math.floor(this.countNum));
                        },
                        complete: function() {
                            $this.text(this.countNum);
                            //alert('finished');
                        }
                    });
            });
            a = 1;
        }
    }
});

if (screen.width > 765) {
    window.onscroll = function() {scrollFunction()};
    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("gotopbtn").style.display = "block";
        } else {
            document.getElementById("gotopbtn").style.display = "none";
        }
    }

    $('#gotopbtn').click(function() {
        var gotopbtnElement=document.getElementById("gotopbtn");
        var rocketFire=gotopbtnElement.getElementsByTagName('b');
        rocketFire[0].style.display = "inline-block";
        setTimeout(function(){  rocketFire[0].style.display = "none"; }, 1100);
        console.warn("rocketFire",rocketFire);
        var body = $("html, body");
        body.stop().animate({scrollTop:0}, 1000, 'swing', function() {
        });
    });
}

$( window ).scroll(function() {
    $( ".counter-section" ).addClass("visibled");
});
$(".search-show-btn").click(function(){
    $(".search-text").toggleClass("slide");
});

$('#main-slider').owlCarousel({
    loop:true,
    margin:10,
    dots:true,
    nav:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    navText:['<div class="slider-arrow-box"><img src="img/arrow-white.svg" /></div>','<div class="slider-arrow-box"><img src="img/arrow-white.svg" /></div>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})

$('#home-partners').owlCarousel({
    items:3,
    loop:true,
    margin:20,
    dots:true,
    nav:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    navText:['<div class="slider-arrow-box"><img src="img/arrow-blue.svg" /></div>','<div class="slider-arrow-box"><img src="img/arrow-blue.svg" /></div>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:5
        },
        1000:{
            items:5
        }
    }
})

/*
$('.play').on('click',function(){
    $('#main-slider').trigger('play.owl.autoplay',[1000])
    $(this).addClass('close');
    $('.stop').removeClass('close');
})
$('.stop').on('click',function(){
    $('#main-slider').trigger('stop.owl.autoplay')
    $(this).addClass('close');
    $('.play').removeClass('close');
})
*/

/*
$('#information-notes').owlCarousel({
    items:1,
    loop:true,
    margin:15,
    dots:true,
    nav:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    navText:['<div class="slider-arrow-box"><img src="img/arrow-white.svg" /></div>','<div class="slider-arrow-box"><img src="img/arrow-white.svg" /></div>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})


$('#lawyers-on-subject').owlCarousel({
    items:5,
    loop:true,
    margin:15,
    dots:true,
    nav:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    navText:['<div class="slider-arrow-box"><img src="img/arrow-blue.svg" /></div>','<div class="slider-arrow-box"><img src="img/arrow-blue.svg" /></div>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:5
        },
        1000:{
            items:5
        }
    }
})
*/

$('#accordionExample').collapse({
    toggle: false
})



$('.owl-next').click(function () {
    var owlNext=document.getElementsByClassName('owl-next');
    var sliderArrowBoxNext=owlNext[0].getElementsByClassName('slider-arrow-box');
    var SliderRocketFire=sliderArrowBoxNext[0].getElementsByTagName('b');
    SliderRocketFire[0].style.display = "inline-block";
    setTimeout(function(){  SliderRocketFire[0].style.display = "none"; }, 200);
})

$('.owl-prev').click(function () {
    var owlPrev=document.getElementsByClassName('owl-prev');
    var sliderArrowBoxNext=owlPrev[0].getElementsByClassName('slider-arrow-box');
    var SliderRocketFire=sliderArrowBoxNext[0].getElementsByTagName('b');
    SliderRocketFire[0].style.display = "inline-block";
    setTimeout(function(){  SliderRocketFire[0].style.display = "none"; }, 200);
})

$('.get-offer-btn').click(function(){
    $('.get-offer').toggleClass('slide-open');
});
$('.get-offer-close').click(function(){
    $('.get-offer').removeClass('slide-open');
});

//greyscalemethod
/**/
/*
$('.img-border-radius').click(function (){
    alert("hover");
    $(this).addClass('grayscale-off');
});

$(document).ready(function () {
    $('.img-border-radius').click(function (){
        alert("hover");
        $(this).addClass('grayscale-off');
    });
    $('.grayscale').onmouseover(function () {
        alert("hover");
        $(this).addClass('grayscale-off');
    });


    $(".grayscale").hover(
        function () {
            alert("hover");
            $(this).addClass("grayscale-off");
        }, function () {
            $(this).removeClass("grayscale-off");
        }
    );
}
*/



//var leftImageDiv = document.getElementsByClassName("left-img")[0];





var leftPhoto=document.getElementById('left50-img');
var rightPhoto=document.getElementById('right50-img');
var rightPhotoTwo=document.getElementById('right50-imgtwo');
var rightPhotoThree=document.getElementById('right50-imgthree');


function leftPositionImage(value){
    var leftImage=document.getElementById(value);

    if(leftImage!=undefined){
        var containerWidth = document.getElementsByClassName("container")[0];
        var windowWidth = window.innerWidth;
        var leftSpace = (windowWidth - containerWidth.offsetWidth ) / 2;
        var leftMınus =leftSpace + 30;
        var att = document.createAttribute("style");  
        leftImage.setAttribute("style", "left:-"+leftMınus+"px");
     
    }

    return;
}





function rightPositionImage(value){
    var rightImage=document.getElementById(value);
    if(rightImage!=undefined){
    var containerWidth = document.getElementsByClassName("container")[0];
    var windowWidth = window.innerWidth;
    var leftSpace = (windowWidth - containerWidth.offsetWidth ) / 2;
    var windowHalfWidth=(windowWidth / 2) - 25;
    rightImage.setAttribute("style", "width:"+windowHalfWidth+"px");
    }
    return;
}


if(window.innerWidth > 786){
    if(leftPhoto!=undefined){
        leftPositionImage('left50-img');
    }
    if(rightPhoto !== undefined){
        rightPositionImage('right50-img');
    }
    if(rightPhotoTwo !== undefined){
        rightPositionImage('right50-imgtwo');
    }
    if(rightPhotoThree !== undefined){
        rightPositionImage('right50-imgthree');
    }
}


window.onresize = function(event) {
    if(window.innerWidth > 786){
    if(leftPhoto!=undefined){
        leftPositionImage('left50-img');
    }
    if(rightPhoto!=undefined){
        rightPositionImage('right50-img');
    }
    if(rightPhotoTwo !== undefined){
        rightPositionImage('right50-imgtwo');
    }
    if(rightPhotoThree !== undefined){
        rightPositionImage('right50-imgthree');
    }
}
};



var productinput = document.getElementById('productinput');
var productText = document.getElementById('producttext');

if(productinput!=undefined)
{
    productinput.addEventListener('input', function(event) {
        if(event.target.value=="")
        {   
            var findBuyer=document.getElementById("findbuyer");
            attr=document.createAttribute('disabled');
            attr.value = "true";   

            findBuyer.setAttributeNode(attr);
        }else {
            document.getElementById("findbuyer").removeAttribute('disabled');
            productText.value = event.target.value.split('').join('');
        }
    });
}
