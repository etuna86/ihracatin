<?php include 'config.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $lang['media_planning']; ?> - <?php echo $lang['ihracatin']; ?></title>
    <meta name="description" content="<?php echo $lang['metadesc_media_planning'] ?>" />
    <?php include 'includes/styles.php'; ?>
</head>
<body>
<?php include 'includes/header.php'; ?>
<section class="pageheader">
    <div class="page-header-content">
        <div class="page-header-content-box">
            <div class="container">
                <div class="page-header-menu">
                    <ul>
                        <li><a href="index.php"><?php echo $lang['mainpage']; ?>&nbsp;-&nbsp;</a></li>
                        <li><a><?php echo $lang['products']; ?>&nbsp;-&nbsp;</a></li>
                        <li><a  class="active"><?php echo $lang['media_planning']; ?></a></li>
                    </ul>
                </div>
                <h1><?php echo $lang['media_pageheader_title']; ?></h1>
                <p><?php echo $lang['media_pageheader_content']; ?></p>
            </div>
        </div>
        <div class="bottom"></div>
    </div>
    <img src="assets/images/pageheaders/medya-planlama.jpg" alt="" />

</section>
<section class="main-content ">
    <div class="tab-box p-0">
            <div class="container">
                <div class="tab-box-inside">
                    <div class="row">
                    <div class="col-md-6">
                            <div class="tab-box-img left-img">
                                <div class="img-line">
                                    <div class="left"></div>
                                    <div class="right"></div>
                                </div>
                                <div class="dots-left">
                                    <img src="assets/images/homepages/homeboxleft.png" alt="">
                                </div>
                                <img id="left50-img" src="assets/images/products/medya-planlama/medya-planlama.jpg" alt="medya pazarlama" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="home-exporters-box">
                                <div class="home-exporters-box-inside">
                                    <h3><?php echo $lang['data_whatisthis']; ?></h3>
                                    <h2><?php echo $lang['media_whatisthis_title']; ?></h2>
                                    <p><?php echo $lang['media_whatisthis_content']; ?></p>
                                    <p>
                                    <?php echo $lang['media_whatisthis_content_two']; ?>
                                    </p>
                                   
                                </div>
                                
                            </div>
                        </div>

                    </div>
                    

                </div>
                
            </div>
        </div>
    </div>
</section>


<section class="main-options">
    <div class="left-absolute"></div>
    <div class="tab-box p-0">
        <div class="container">
            <div class="tab-box-inside">
                <div class="row">
                    <div class="col-md-6">
                        <div class="home-exporters-box">
                            <div class="home-exporters-box-inside">
                                <h3><?php echo $lang['main_features']; ?></h3>
                                <h2><?php echo $lang['media_mainfeatures_title']; ?></h2>
                                <p><?php echo $lang['media_mainfeatures_content']; ?>
</p>
                                <p>
                                    <?php echo $lang['media_mainfeatures_content_two']; ?> 
                                </p>
                                <div class="try-now">
                                    <a href="#" data-toggle="modal" data-target="#exampleModal" ><?php echo $lang['quick_application']; ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="tab-rules">
                            <div class="rules-box">
                                <div class="rules-icon">
                                    <i class="fa fa-globe-americas" aria-hidden="true"></i>
                                </div>
                                <div class="rules-desc">
                                    <p>
                                        <?php echo $lang['media_rules_content_one']; ?> 
                                    </p>
                                </div>
                            </div>
                            <div class="rules-box">
                                <div class="rules-icon">
                                    <i class="fa fa-globe-americas" aria-hidden="true"></i>
                                </div>
                                <div class="rules-desc">
                                    <p>
                                        <?php echo $lang['media_rules_content_two']; ?> 
                                    </p>
                                </div>
                            </div>
                            <div class="rules-box">
                                <div class="rules-icon">
                                    <i class="fa fa-globe-americas" aria-hidden="true"></i>
                                </div>
                                <div class="rules-desc">
                                    <p>
                                        <?php echo $lang['media_rules_content_three']; ?> 
                                    </p>
                                </div>
                            </div>
                            <div class="rules-box">
                                <div class="rules-icon">
                                    <i class="fa fa-globe-americas" aria-hidden="true"></i>
                                </div>
                                <div class="rules-desc">
                                    <p>
                                    <?php echo $lang['media_rules_content_four']; ?> 
                                    </p>
                                </div>
                            </div>
                            <div class="rules-box">
                                <div class="rules-icon">
                                    <i class="fa fa-globe-americas" aria-hidden="true"></i>
                                </div>
                                <div class="rules-desc">
                                    <p>
                                    <?php echo $lang['media_rules_content_five']; ?>  
                                    </p>
                                </div>
                            </div>
                            <div class="rules-box">
                                <div class="rules-icon">
                                    <i class="fa fa-globe-americas" aria-hidden="true"></i>
                                </div>
                                <div class="rules-desc">
                                    <p>
                                        <?php echo $lang['media_rules_content_six']; ?> 
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>  
            </div>
        </div>
    </div>  
</section>

<section class="additional-features-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h2><?php echo $lang['additional_features']; ?></h2>
                <div class="additional-features">
                  
                    <div class="features-box">
                        <div class="features-img">
                            <div class="features-media one"></div>
                        </div>
                        <h3><?php echo $lang['media_features_title_one']; ?></h3>
                        <p><?php echo $lang['media_features_content_one']; ?></p>
                    </div>
                    <div class="features-box">
                        <div class="features-img">
                            <div class="features-media two"></div>
                        </div>
                        <h3><?php echo $lang['media_features_title_two']; ?></h3>
                        <p><?php echo $lang['media_features_content_two']; ?></p>
                    </div>
                    <div class="features-box">
                        <div class="features-img">
                            <div class="features-media three"></div>
                        </div>
                        <h3><?php echo $lang['media_features_title_three']; ?></h3>
                        <p><?php echo $lang['media_features_content_three']; ?></p>
                    </div>
                    <div class="features-box">
                        <div class="features-img">
                            <div class="features-media four"></div>
                        </div>
                        <h3><?php echo $lang['media_features_title_four']; ?></h3>
                        <p><?php echo $lang['media_features_content_four']; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>

<?php include 'includes/footer.php'; ?>

<?php include 'includes/scripts.php'; ?>
</body>
</html>
